package cosita;

import java.util.Scanner;

public class Ejercicio3 {

	public static void main(String[] args) {
		/*
		 * Implementa un programa en Java que reciba dos par�metros de tipo entero:
		 * distancia y tiempo. El programa mostrar� por pantalla la aceleraci�n obtenida
		 * a partir de estas variables. La formula de la aceleraci�n es: a= d/t. Un
		 * ejemplo de ejecuci�n ser�a el siguiente:
		 */

		Scanner sc = new Scanner(System.in);
		// Inicializamos teclado
		System.out.println("Introduzca un n�mero entero para representar los segundos:");
		int t = sc.nextInt();
		// Solicitamos el n�mero de segundos y almacenamos la variable.
		System.out.println("Introduzca un n�mero entero para representar los metros:");
		int d = sc.nextInt();
		// Solicitamos el n�mero de metros y almacenamos la variable.

		double a = (double) Math.round(((double) d / t) * 100) / 100;
		// Realizamos la divisi�n, teniendo en cuenta el redondeo a dos cifras.
		System.out.println("La aceleraci�n resultante de " + d + " segundos en " + t + " metros es de " + a + " m/s.");
		sc.close();
		// Imprimimos el resultado y cerramos teclado.
	}

}
