package cosita;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		/*
		 * Implemente un programa en Java que acepte un n�mero entero por teclado,
		 * representando un n�mero en cent�metros, y muestre por pantalla su
		 * descomposici�n en metros y kil�metros. NOTA: Se debe utilizar el operador
		 * m�dulo.
		 */
		Scanner sc = new Scanner(System.in);
		// Inicializamos teclado.
		System.out.println("Introduzca un n�mero entero en cent�metros: ");
		int cm = sc.nextInt();
		// Solicitamos al usuario un n�mero entero y lo introducimos en la variable cm.
		int cmAux = cm;
		// Creamos un auxiliar para guardar los cm y poder machacar la variable.
		cm = cm % 100;
		// Operaci�n m�dulo para que nos d� los cm restantes.
		int m = cmAux / 100;
		// Convertimos los cm a metros.
		int mAux = m;
		// De nuevo, Aux para almacenar los metros y poder machacar la variable.
		int km = mAux / 1000;
		// Convertimos los metros a km.
		m = m % 1000;
		// Sacamos el resto de los metros.
		System.out.println("El n�mero de cent�metros que ha introducido corresponde a:\n" + km + " Kil�metros.\n" + m
				+ " Metros.\n" + cm + " Cent�metros.");
		sc.close();
		// Para finalizar, imprimimos el resultado y cerramos teclado.
	}

}
