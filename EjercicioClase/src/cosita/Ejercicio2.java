package cosita;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		/*
		 * Implemente un programa en Java que resuelva la siguiente f�rmula para
		 * resolver con qu� fuerza se atraen dos cuerpos
		 */
		Scanner sc = new Scanner(System.in);
		final double G = 6.673e-11;
		// Inicializamos teclado y declaramos constante gravitatoria.

		System.out.println("Introduzca la masa del primer objeto en gramos:");
		double m1 = sc.nextInt();
		// Solicitamos al usuario la masa del primer objeto y la almacenamos en un
		// double.
		System.out.println("Introduzca la masa del segundo objeto en gramos:");
		double m2 = sc.nextInt();
		// Solicitamos al usuario la masa del segundo objeto y la almacenamos de nuevo,
		// en un double.
		System.out.println("Introduzca la distancia en cent�metros que hay entre el centro de ambos objetos:");
		double r = sc.nextInt();
		// Finalmente solicitamos la distancia entre los objetos y la almacenamos.
		double f = G * ((m1 * m2) / Math.pow(r, 2));
		// Aplicamos la f�rmula, usando Math.pow para elevar la R y la almacenamos en
		// una nueva variable.
		System.out.println("La fuerza con la que ambos objetos se atraen es de " + f + " newtons.");
		sc.close();
		// Imprimimos el resultado y cerramos teclado.
	}

}
