package examen2;

import java.util.Arrays;
import java.util.Scanner;

public class Canicas {

	public static void main(String[] args) {
		/*
		 * Realiza un programa para realizar una b�squeda en una bolsa de canicas. El
		 * array se inicializar� con 28 canicas de distintos colores(usar tipo char
		 * preferentemente). Los colores posibles ser�n: Rojo Fuego, Verde Hoja,
		 * Cristal, Plata, Oro, Diamante y Perla. El usuario introducir� un color y se
		 * le devolver� el n�mero de canicas de ese color que hay en el array.
		 */

		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int tipoCanica[] = new int[] { 1, 2, 3, 4, 5, 6, 7 };
		//Array de canicas por c�digo.
		int canica[] = new int[24];
		//Array con las canicas que van en la bolsa.
		String bolsa[] = new String[24];
		//Array para ponerle el nombre a las canicas.
		int buscar = 0;
		//Variable para almacenar el n�mero por el que quiere buscar el usuario.
		int contadorCanica = 0;
		//Contador para las canicas de ese color.
		String canicaElegida = " ";

		for (int i = 0; i < canica.length; i++) {
			canica[i] = tipoCanica[(int) (Math.random() * tipoCanica.length)];
			//Este for almacena de forma autom�tica canicas en la bolsa hasta llegar al largo del array. Los tipos introducidos son aleatorios.
		}
		for (int i = 0; i < bolsa.length; i++) {
			//Este for transforma el c�digo num�rico en un String con el nombre de la canica al que corresponde.
			if (canica[i] == 1) {
				bolsa[i] = "Rojo Fuego";
			} else if (canica[i] == 2) {
				bolsa[i] = "Verde Hoja";

			} else if (canica[i] == 3) {
				bolsa[i] = "Cristal";

			} else if (canica[i] == 4) {
				bolsa[i] = "Plata";

			} else if (canica[i] == 5) {
				bolsa[i] = "Oro";

			} else if (canica[i] == 6) {
				bolsa[i] = "Diamante";

			} else if (canica[i] == 7) {
				bolsa[i] = "Perla";

			}
		}

		System.out.println(
				"�Qu� tipo de canica quiere buscar?\n1. Rojo Fuego.\n2. Verde Hoja.\n3. Cristal.\n4. Plata.\n5. Oro.\n6. Diamante.\n7. Perla.");
		buscar = sc.nextInt();
		//Solicitamos al usuario el tipo de canica a buscar y lo almacenamos.

		for (int i = 0; i < canica.length; i++) {
			//Este for recorre el array de canicas.
			if (canica[i] == buscar) {
				contadorCanica++;
				//Aumenta en contador por cada coincidencia del color deseado.
			}
		}
		if (buscar == 1) {
			canicaElegida = "Rojo Fuego";
		} else if (buscar == 2) {
			canicaElegida = "Verde Hoja";

		} else if (buscar == 3) {
			canicaElegida = "Cristal";

		} else if (buscar == 4) {
			canicaElegida = "Plata";

		} else if (buscar == 5) {
			canicaElegida = "Oro";

		} else if (buscar == 6) {
			canicaElegida = "Diamante";

		} else if (buscar == 7) {
			canicaElegida = "Perla";
		}
		//Ifs para que el syso de despu�s quede m�s bonito.
		System.out.println("Bolsa de canicas: "+Arrays.deepToString(bolsa));
		System.out.println("En la bolsa hay "+contadorCanica+" canicas del color "+canicaElegida+".");
		//Mostramos la bolsa y cu�ntas canicas hay del color deseado.
		sc.close();
		//Cerramos teclado.

	}

}
