package examen2;
import java.util.Scanner;

public class Empresa {

	public static void main(String[] args) {
		/*
		 * Crear un programa para la gesti�n de los empleados de una empresa. Los datos
		 * de los empleados de la empresa se almacenar�n en una matriz donde cada fila
		 * representa los datos de un empleado. Los datos de un empleado vendr�n dados
		 * por un n�mero entero que identifique el Departamento al que pertenece el
		 * empleado, el salario bruto que recibe y su grado de satisfacci�n con la
		 * empresa. El programa debe leer por teclado el identificador de un
		 * Departamento y mostrar en la pantalla el grado de satisfacci�n y el salario
		 * medio de los empleados de dicho departamento.
		 */
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		
		int plantilla[][] = new int [][] {
			{1,1400,0},
			{3,2000,5},
			{2,2100,1},
			{1,1250,4},
			{1,3050,4},
			{3,2300,3},
			{2,1780,5},
			{2,1230,0},
			{3,2500,1},
			{1,1750,3},
		};
		//Array con la informaci�n de la plantilla, est� hardcodeado #SorryNotSorry.
		
		double sumaSalario = 0;
		double mediaSalario = 0;
		//Variables para calcular la media del salario.
		double sumaSatisf = 0;
		double mediaSatisf = 0;
		//Variables para calcular la media de satisfacci�n.
		int contadorTrabaj = 0;
		//Un contador para la media.
		int resp = 0;
		//Variable para almacenar la respuesta del usuario.
		String departamento = " ";

		System.out.println("Departamento   Salario        Satisfacci�n");
		//For para imprimir los datos del departamento.
		for (int i=0;i<plantilla.length;i++) {
			for (int j=0;j<plantilla[i].length;j++) {
				if(j==2) {
					System.out.println(plantilla[i][j]+"               ");
				}else {
					System.out.print(plantilla[i][j]+"               ");
				}
			}
		}
		do {
			System.out.println("Introduzca el n�mero de departamento sobre el que quiere recibir informaci�n: \n1. Recursos Humanos.\n2. Departamento Comercial.\n3. BackOffice.");
			//Este bucle solicita al usuario el n�mero del departamento sobre el que quiere buscar informaci�n y se repite hasta que introduce uno de los n�meros v�lidos.
			resp = sc.nextInt();
			if (resp!=1 || resp!=2 || resp!=3) {
				System.out.println("Por favor, elija uno de los departamentos de la empresa.");
			}
		}while(resp!=1 && resp!=2 && resp!=3);
		for (int i=0;i<plantilla.length;i++) {
			//Este for recorre el array principal.
			for (int j = 0;j<plantilla[i].length;j++) {
				//Este for recorre los sub array.
				if (plantilla[i][j]==resp && j==0) {
					sumaSalario += plantilla[i][j+1];
					sumaSatisf += plantilla[i][j+2];
					contadorTrabaj++;	
					//Si encuentra una coincidencia en la primera columna (la de departamento), suma los resultados de la segunda y tercera columnas a las variables correspondientes y aumenta el contador.
				}
			}
		}			
		mediaSalario = (double) Math.round((double) (sumaSalario/contadorTrabaj)*100)/100;
		mediaSatisf = (double) Math.round((double) (sumaSatisf/contadorTrabaj)*100)/100;
		//Calculamos la media de ambos valores, no es doble casteo, es que el math round funciona como funciona.
		if (resp==1) {
			departamento = "Recursos Humanos";
		}
		if (resp==2) {
			departamento = "Departamento Comercial";
		}
		if (resp==3) {
			departamento = "BackOffice";
		}
		//Ifs para que el syso de despu�s quede m�s bonito.
		System.out.println("La media de salario de los trabajadores del departamento de "+departamento+" es de "+mediaSalario);
		System.out.println("La media de satisfacci�n de los trabajadores del departamento de "+departamento+" es de "+mediaSatisf);
		//Imprimimos los resultados de la consulta.
	sc.close();
	//Cerramos teclado.
	
	/*Recuerda que el eclipse se me quedaba mo�eco cuando lo lanzaba durante varios segundos
	 * as� que pls se paciente o haz click en el c�digo despu�s de lanzarlo, si aun as� no te
	 * funciona manda un ticket a https://support.riotgames.com/hc/es. A ellos se les da de
	 * puta madre arreglar c�digo.
	 */
	}
}
