package examen2;

import java.util.Arrays;
import java.util.Scanner;

public class Carniceria {

	public static void main(String[] args) {
		/*
		 * Realiza un programa que va a simular la cola de espera de una carnicer�a. El
		 * funcionamiento del programa mostrar� la cola de la carnicer�a y preguntar� al
		 * usuario si puede pasar el siguiente cliente. En caso afirmativo, se repite la
		 * ejecuci�n del programa, en caso negativo se termina la ejecuci�n del mismo.
		 * Considera que la carnicer�a tiene un tama�o de cola de 10 personas.
		 */
		
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int cola[] = new int[] {1,2,3,4,5,6,7,8,9,10};
		//Array para la cola, inicializado de 1 a 10 porque siempre ser� la misma al abrir.
		boolean salir = false;
		//Bool para salir del bucle principal.
		char resp = ' ';
		//Char para almacenar la respuesta del usuario.
		
		do {
			//Bucle central, salir de �l implica salir del programa.
			System.out.println("Cola de espera a la carnicer�a: "+Arrays.toString(cola));
			//Imprimimos el estado actual de la cola.
			System.out.println("�Desea que pase el siguiente cliente? (S/N)");
			//Solicitamos imput al usuario.
			do {
				//Bucle secundario.
			resp=sc.next().charAt(0);
			//Almacenamos la respuesta.
			if(resp=='N') {
				//Si es negativa, cambiamos la flag para salir del bucle central.
				salir = true;
			}if(resp=='S') {
				//Si es positiva, el programa mueve la cola y el bucle seguir� ejecut�ndose.
				System.out.println("Que pase el siguiente cliente.");
				for (int i=0; i<cola.length;i++) {
					cola[i]++;
				}
			}else {
				//En caso de que introduzca alguna otra cosa, volvemos al inicio del bucle secundario hasta que introduzca una de las dos respuestas posibles.
				System.out.println("Por favor, indique si la cola debe o no continuar.");
			}
			}while (resp != 'N' && resp != 'S');
		}while(!salir);
		//Cuando la flag para salir est� en true, saldremos del bucle principal.
		System.out.println("La carnicer�a cierra sus puertas. Les esperamos ma�ana en horario de 7h a 15h");
		//Mensaje de despedida.
		sc.close();
		//Cerramos teclado.

	}

}
