package arrays;

import java.util.Scanner;

public class Sudoku {

	public static void main(String[] args) {
		// Programa que comprueba si un Sudoku est� correcto.
		Scanner sc = new Scanner(System.in);
		
		final int FILA = 9;
		final int COLUMN = 9;
		int sudoku[][] = new int[FILA][COLUMN];
		String dibujo[][] = new String[FILA][COLUMN];
		boolean comprobar = true;
		int numComprob = 0;


		sudoku[0][0]=8; sudoku[0][1]=2; sudoku[0][2]=7;   sudoku[0][3]=1; sudoku[0][4]=5; sudoku[0][5]=4;   sudoku[0][6]=3; sudoku[0][7]=9; sudoku[0][8]=6;
		sudoku[1][0]=9; sudoku[1][1]=6; sudoku[1][2]=5;   sudoku[1][3]=3; sudoku[1][4]=2; sudoku[1][5]=7;   sudoku[1][6]=1; sudoku[1][7]=4; sudoku[1][8]=8;
		sudoku[2][0]=3; sudoku[2][1]=4; sudoku[2][2]=1;   sudoku[2][3]=6; sudoku[2][4]=8; sudoku[2][5]=9;   sudoku[2][6]=7; sudoku[2][7]=5; sudoku[2][8]=2;
		
		sudoku[3][0]=5; sudoku[3][1]=9; sudoku[3][2]=3;   sudoku[3][3]=4; sudoku[3][4]=6; sudoku[3][5]=8;   sudoku[3][6]=2; sudoku[3][7]=7; sudoku[3][8]=1;
		sudoku[4][0]=4; sudoku[4][1]=7; sudoku[4][2]=2;   sudoku[4][3]=5; sudoku[4][4]=1; sudoku[4][5]=3;   sudoku[4][6]=6; sudoku[4][7]=8; sudoku[4][8]=9;
		sudoku[5][0]=6; sudoku[5][1]=1; sudoku[5][2]=8;   sudoku[5][3]=9; sudoku[5][4]=7; sudoku[5][5]=2;   sudoku[5][6]=4; sudoku[5][7]=3; sudoku[5][8]=5;
		
		sudoku[6][0]=7; sudoku[6][1]=8; sudoku[6][2]=6;   sudoku[6][3]=2; sudoku[6][4]=3; sudoku[6][5]=5;   sudoku[6][6]=9; sudoku[6][7]=1; sudoku[6][8]=4;
		sudoku[7][0]=1; sudoku[7][1]=5; sudoku[7][2]=4;   sudoku[7][3]=7; sudoku[7][4]=9; sudoku[7][5]=6;   sudoku[7][6]=8; sudoku[7][7]=2; sudoku[7][8]=3;
		sudoku[8][0]=2; sudoku[8][1]=3; sudoku[8][2]=9;   sudoku[8][3]=8; sudoku[8][4]=4; sudoku[8][5]=1;   sudoku[8][6]=5; sudoku[8][7]=6; sudoku[8][8]=7;
		
		
		
		/*for (int j = 0; j < sudoku.length; j++) {
			// Este for recorre el array m�ltiple 
			for (int i = 0; i < sudoku[j].length; i++) {
				// Este array recorre los sub array.
				System.out.println("Introduzca la posici�n "+(j+1)+" de la columna "+(i+1)+".");
				sudoku[j][i] = sc.nextInt();
			}
		}*/
		
		
		for (int j = 0; j < sudoku.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < sudoku[j].length; i++) {
				// Este array recorre los sub array.
				numComprob = sudoku[j][i];

					for (int k = i+1; k < sudoku[j].length; k++) {
						// Este array recorre los sub array.
						if (numComprob==sudoku[j][k]) {
							comprobar = false;	
						}
					}
					for (int h = j+1; h<1;h++) {
						for (int l=0; l<1;l++) {
							if (numComprob==sudoku[h][l]) {
								comprobar = false;
							}
						}
					}
					for (int m = 0; m<2;m++) {
						for (int n= 0+1; n<1;n++) {
							if (numComprob==sudoku[m][n]) {
								comprobar=false;
							}
						}
						
					}
				}

			}
		if(comprobar) {
			System.out.println("El sudoku es correcto.");
		
		
		for (int j = 0; j < dibujo.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < dibujo[j].length; i++) {
				// Este array recorre los sub array.
			dibujo[j][i]="["+sudoku[j][i]+"]";
			}
		}
		
		for (int j = 0; j < dibujo.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < dibujo[j].length; i++) {
				// Este array recorre los sub array.
				if (i==3 || i==6) {
					System.out.print(" ");
				}
				if (i==8) {
					System.out.println(dibujo[j][i]);
					//Imprimimos los saltos de l�nea al final de cada fila.
				}
				else {
					System.out.print(dibujo[j][i]);
					//Las vac�as simplemente los corchetes.
				}
			}
		}
		}else {
			System.out.println("El sudoku presenta errores.");
		}
		sc.close();
	}

}
