package arrays;

public class Signos {

	public static void main(String[] args) {
		// Imprimir el array en pizarra.

		char[][] dibujo = new char[5][7];

		for (int j = 0; j < dibujo.length; j++) {
			// Este for recorre el array m�ltiple
			if (j == 0 || j == 4) {
				for (int i = 0; i < dibujo[j].length; i++) {
					// Este array recorre los sub array.
					dibujo[j][i] = '-';
					// Almacenamos un - si las l�neas son la primera o la �ltima.
				}
			} else {
				for (int i = 0; i < dibujo[j].length; i++) {
					// Este array recorre los sub array.
					dibujo[j][i] = '+';
					//Almacenamos un + si no lo son.

				}
			}

		}
		for (int j = 0; j < dibujo.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < dibujo[j].length; i++) {
				// Este array recorre los sub array.
				if (i == 6) {
					//Si llegamos al signo 7, hacemos salto de l�nea.
					System.out.println(dibujo[j][i]);
				} else {
					System.out.print(dibujo[j][i]);
				}
			}
		}
	}
}
