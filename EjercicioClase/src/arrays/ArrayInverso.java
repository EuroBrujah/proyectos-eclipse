package arrays;

import java.util.Scanner;

public class ArrayInverso {

	public static void main(String[] args) {
		// M�todo que recibe un array y lo devuelve en orden inverso.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		System.out.println("Introduzca el tama�o del array: ");
		int n[] = new int [sc.nextInt()];
		//Solicitamos al usuario el tama�o del array e inicializamos un array con esas posiciones.
		
		for (int i=n.length-1; i>=0;i--) {
			n[i] = i+1;
			//Este for recorre el array de forma inversa y asigna a cada n�mero su posici�n +1.
		}
		
		for (int i=0; i<n.length;i++) {
			System.out.println(n [i]);
			//Imprimimos los resultados.
		}
		sc.close();
		//Cerramos teclado.
	}

}
