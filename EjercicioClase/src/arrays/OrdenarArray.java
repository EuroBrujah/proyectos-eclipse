package arrays;

public class OrdenarArray {

	public static void main(String[] args) {
		// Ordenar un array de menor a mayor
		int n[] = new int[] { 13, 5, 55, 21, 3, 8, 0, 2, 34, 1 };
		int n2[] = new int[n.length];
		//Inicializamos un array con los n�meros que queramos y otro vac�o.
		int mayor = 0;
		//Variable para ayudarnos a ordenar el array y excluir n�meros.
		
		for (int j = n2.length - 1; j > 0; j--) {
			//Recorremos el array vac�o
			for (int i = 0; i < n.length; i++) {
				if (n[i] > mayor) {
					mayor = n[i];
					//Usamos este for para ir almacenando el mayor y poder ordenarlo.

				}
			}
			for (int h = 0; h < n.length; h++) {
				if (n[h] == mayor) {
					n[h] = 0;
					//Una vez que hemos encontrado el mayor, lo sustituimos por un 0.
				}
			}

			n2[j] = mayor;
			//Almacenamos el mayor en la �ltima posici�n, de esta forma los 0 quedan exclu�dos si no hab�a ninguno en el array original.
			mayor = 0;
			//Reiniciamos la variable.
		}
		for (int i = 0; i < n2.length; i++) {
			System.out.println(n2[i]);
			// Imprimimos los resultados.
		}
	}

}
