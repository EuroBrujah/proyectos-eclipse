package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MultiArray {

	public static void main(String[] args) {
		// El usuario rellena dos array anidados.

		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int[][] mArray = new int[2][3];
		//Declaramos un array m�ltiple de enteros compuesto por dos array con 3 posiciones cada uno.

		for (int j = 0; j < mArray.length; j++) {
			//Este for recorre el array m�ltiple
			for (int i = 0; i < mArray[j].length; i++) {
				//Este array recorre los sub array.
				System.out.println("Introduzca un n�mero para la posici�n " + (i + 1) + " del array " + (j + 1) + ".");
				mArray[j][i] = sc.nextInt();
				//Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del array m�ltiple (j) y del sub array (i) el n�mero.
			}
		}

		/*System.out.println("N�meros");
		for (int j = 0; j < mArray.length; j++) {
			for (int i = 0; i < mArray[j].length; i++) {
				System.out.println(mArray[j][i]);
				//Este for lee todas las posiciones e imprime los resultados.*/
		System.out.println(Arrays.deepToString(mArray));
		//As� te lo imprime presioso.
		sc.close();
			}
		//Cerramos teclado.
	}


