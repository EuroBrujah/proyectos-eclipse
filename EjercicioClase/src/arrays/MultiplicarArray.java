package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MultiplicarArray {

	public static void main(String[] args) {
		// El usuario indica tama�o de columna y fila de ambos array, y adem�s los
		// rellenar, despu�s multiplicamos filas por columnas.

		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		System.out.println("Introduce el n�mero de filas de ambos array: ");
		int fila = sc.nextInt();
		System.out.println("A continuaci�n indique el n�mero de columnas: ");
		int columna = sc.nextInt();
		//Solicitamos al usuario el n�mero de filas y columnas y las almacenamos.

		int mArray1[][] = new int[fila][columna];
		int mArray2[][] = new int[fila][columna];
		int mArray3[][] = new int[fila][columna];
		//Inicializamos los array con el n�mero de filas y columnas introducidas.

		for (int j = 0; j < mArray1.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray1[j].length; i++) {
				// Este array recorre los sub array.
				System.out.println("Introduzca un n�mero para la posici�n " + (i + 1) + " del array " + (j + 1)
						+ " del primer array m�ltiple.");
				mArray1[j][i] = sc.nextInt();
				// Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del
				// array m�ltiple (j) y del sub array (i) el n�mero.
			}
		}
		for (int j = 0; j < mArray2.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray2[j].length; i++) {
				// Este array recorre los sub array.
				System.out.println("Introduzca un n�mero para la posici�n " + (i + 1) + " del array " + (j + 1)
						+ " del segundo array m�ltiple.");
				mArray2[j][i] = sc.nextInt();
				// Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del
				// array m�ltiple (j) y del sub array (i) el n�mero.
			}
		}
		System.out.println(Arrays.deepToString(mArray1));
		System.out.println(Arrays.deepToString(mArray2));

		for (int j = 0; j < mArray3.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray3[j].length; i++) {
				// Este array recorre los sub array.
				mArray3[i][j] = mArray1[j][i] * mArray2[i][j];
				// Rellenamos el array por columnas con el resultado de multiplicar fila de array1 por columna de array2.

			}
		}
		System.out.println(Arrays.deepToString(mArray3));
		//Imprimimos resultados.
		sc.close();
	}
}