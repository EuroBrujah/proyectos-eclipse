package arrays;

import java.util.Scanner;

public class Ratas {

	public static void main(String[] args) {
		// Escribir un programa que lea el peso de cada una de las 20 ratas de laboratorio del
		//departamento de Biolog�a de la Nutrici�n. Calcular el peso medio de las ratas
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		double ratas[] = new double [] {200, 150, 350, 400, 350, 275, 150, 200, 180, 230, 350, 250, 375, 125, 250, 275, 175, 200, 300, 100};
		double ratasAux[] = new double [] {200, 150, 350, 400, 350, 275, 150, 200, 180, 230, 350, 250, 375, 125, 250, 275, 175, 200, 300, 100};
		//Declaramos un array con el peso de las ratas y un array auxiliar que nos servir� para calcular el n�mero de ratas por peso.
		double peso = 0;
		double media = 0;
		//Declaramos una variable para el peso total y otro para la media.
		int contador = 1;
		//Inicializamos un contador para el n�mero de ratas del mismo peso.
				for (int i=0;i<ratas.length;i++) {
					peso += ratas[i];
					//For para sumar el peso total de las ratas.
				}
				media = peso/20;
				//Calculamos la media de peso entre las veinte ratas.
				
				for (int i=0;i<ratasAux.length;i++){
					//For que recorre el array auxiliar.
					for (int j=i+1; j<ratasAux.length;j++) {
						//For que compara la posici�n del array con todas las posiciones siguientes.
						if (ratasAux[i]==ratasAux[j]) {
							contador++;
							ratasAux[j]=0;
							//Si encuentra una coincidencia, aumenta el contador y elimina la segunda ocurrencia de peso repetido.
						}
					}
					if(ratasAux[i]!=0) {
						System.out.println("Hay "+contador+" ratas que pesan "+ratasAux[i]+".");
						//Si la posici�n no ha sido eliminada, imprime peso y cantidad de ratas que lo tienen.
						}
					contador=1;	
					//Reiniciamos el contador.
				}
				System.out.println("El peso total de las ratas es de "+peso+" gramos. La media de su peso es de "+media+" gramos.");
				//Imprimimos el peso total de las ratas y su media.
				sc.close();
				//Cerramos teclado.
		}
	}

