package arrays;

import java.util.Scanner;

public class Posici�nArray {

	public static void main(String[] args) {
		// Introducir un n�mero y decir en qu� posici�n se encuentra. Si no est�, sacamos un -1.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int n[] = new int[] {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
		//Inicializamos un array.
		boolean bool = false;
		//Booleano para almacenar si el nombre ha sido encontrado.
		int comprobar = 0;
		//Variable para almacenar el n�mero a comprobar.
		int contador = 1;
		//Contador para la posici�n del n�mero en el array.
		
		System.out.println("Introduzca un n�mero para saber si est� en el array: ");
		comprobar = sc.nextInt();
		//Solicitamos al usuario un n�mero y lo almacenamos.
		for (int i=0; i<n.length;i++) {
			if (comprobar!=n[i] && !bool) {
				contador++;
				//Este for recorre el array, mientras no encuentre el n�mero, aumenta el contador.
			}
			else if (comprobar==n[i]) {
				bool=true;
				//Si encuentra el n�mero, cambia el bool, adem�s esto har� que el contador deje de aumentar.
			}
		}
		if (bool) {
			System.out.println("El n�mero "+comprobar+" est� inclu�do en el array. En la posici�n "+contador+".");
			//Si ha encontrado el n�mero, devuelve su posici�n.
		}else {
			System.out.println("-1");
		}
		sc.close();
		//Cerramos teclado�
	}

}
