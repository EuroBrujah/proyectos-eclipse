package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Loteria {

	public static void main(String[] args) {
		/*
		 * Escriba un programa que lea desde teclado 6 n�meros correspondientes a la
		 * combinaci�n ganadora de la loter�a primitiva. A continuaci�n, el programa
		 * leer� otros 6 n�meros correspondientes a un boleto con el que se participa en
		 * dicho sorteo. Finalizada la lectura, el programa deber� de indicar el n�mero
		 * de aciertos del boleto cuyos n�meros hemos introducido. Ejemplo: si la
		 * combinaci�n ganadora es 3 6 4 8 9 23 el boleto 8 6 23 5 9 1 tiene 2 aciertos.
		 */
		
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado
		
		int boletoGanador[] = new int[6];
		for (int i=0; i<boletoGanador.length;i++) {
			boletoGanador[i] = (int) (0+Math.random()*10);
		}
		System.out.println(Arrays.toString(boletoGanador));
		//Declaramos un array con 6 n�meros aleatorios de 0 a 9.
		int boletoUsuario[] = new int[6];
		//Declaramos un segundo array con 6 n�meros que rellenar� el usuario.
		int aciertos = 0;
		//Inicializamos un contador para los aciertos.
		
		System.out.println("Introduzca los seis n�meros de su boleto de loter�a, uno a uno: ");
		for (int i = 0;i<boletoUsuario.length;i++) {
			boletoUsuario[i] = sc.nextInt();
			//Solicitamos al usuario que introduzca los n�meros que utilizaremos para el segundo array, usando un for para rellenar las distintas posiciones.
		}
		for (int i=0;i<boletoGanador.length;i++) {
			if (boletoGanador[i] == boletoUsuario[i]) {
				aciertos++;
				//Este for recorre las seis posiciones de ambos array compar�ndolas, en caso de que sean iguales sube el contador de aciertos.
			}
		}
		System.out.println("El n�mero premiado es el "+Arrays.toString(boletoGanador)+". Su boleto "+Arrays.toString(boletoUsuario)+" tiene "+aciertos+" aciertos.");
		sc.close();
		//Imprimimos los resultados y cerramos teclado.
	}

}
