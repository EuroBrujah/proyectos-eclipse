package arrays;

import java.util.Scanner;

public class EliminarPosici�n {

	public static void main(String[] args) {
		// Pedir una posici�n al usuario, borrar ese elemento del array.
		Scanner sc = new Scanner(System.in);
		//Inicializamos scanner.
		int n[] = new int[] {0, 1, 2, 3, 5, 8, 13, 21, 34, 55};
		int n2[] = new int[n.length-1];
		//Inicializamos un array con los n�meros que queramos y otro vac�o con una posici�n menos que el original, para almacenar todo excepto el n�mero que
		//queramos eliminar.
		int mayor = 0;
		//Variable para ayudarnos a ordenar el array y excluir un n�mero.
		
		System.out.println("Introduzca la posici�n que quiere eliminar del siguiente array: {"+n[0]+", "+n[1]+", "+n[2]+", "+n[3]+", "+n[4]+", "+n[5]+", "+n[6]+", "+n[7]+", "+n[8]+", "+n[9]+"}");
		int borrar = sc.nextInt()-1;
		//Solicitamos al usuario la posici�n a eliminar y la almacenamos (con -1, ya que los array empiezan en la posici�n 0.
		
		for (int i=0; i<n.length;i++) {
			if (borrar==i) {
				n[i] = 0;
				//Este for busca la posici�n indicada y la sustituye por un 0.
			}
		}
		for (int j = n2.length-1; j > 0; j--) {
			//Recorremos el array vac�o
			for (int i = 0; i < n.length; i++) {
				if (n[i] > mayor) {
					mayor = n[i];
					//Usamos este for para ir almacenando el mayor y poder ordenarlo.

				}
			}
			for (int h = 0; h < n.length; h++) {
				if (n[h]==mayor) {
					n[h] = 0;
					//Una vez que hemos encontrado el mayor, lo sustituimos por un 0.
				}
			}


			
			n2[j]=mayor;
			//Almacenamos el mayor en la �ltima posici�n, de esta forma los 0 quedan exclu�dos si no hab�a ninguno en el array original.
			mayor=0;
			//Reiniciamos la variable.
			
		}
		for (int i = 0; i < n2.length; i++) {
			System.out.println(n2[i]);
			// Imprimimos los resultados.
		}
sc.close();
//Cerramos teclado.
	}

}
