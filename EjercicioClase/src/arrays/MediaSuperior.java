package arrays;

import java.util.Scanner;

public class MediaSuperior {

	public static void main(String[] args) {
		// Dise�a un programa que pida al usuario 5 n�meros reales que guardar� en un
		// array y luego calcula su media y muestra cual de los n�meros est� por encima
		// de esta.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		double n[] = new double[5];
		//Inicializamos un array con 5 posiciones.
		double suma = 0;
		double media = 0;
		//Inicializamos dos variables para guardar la suma y la media de los n�meros.
		
		System.out.println("Introduzca 5 n�meros reales: ");
		for (int i = 0; i < n.length; i++) {
			n[i] = sc.nextDouble();
			//For que recorre las posiciones del array y las va rellenando con input del usuario.
		}

		for (int i = 0; i < n.length; i++) {
			suma += n[i];
			// Este for recorre todas las posiciones del array y suma todo a la variable
			// suma.
		}
		media = (double) Math.round((suma / n.length) * 100) / 100;
		System.out.println("La media de los n�meros introducidos es: " + media);
		//Dividimos la suma por el largo del array para sacar media e imprimimos.
		System.out.println("Los n�meros superiores a la media son: ");
		for (int i = 0; i < n.length; i++) {
			if (n[i] > media) {
				System.out.println(n[i]);
				//For que recorre las posiciones del array e imprime las mayores a la media.
			}
		}
		sc.close();
		//Cerramos teclado.

	}

}
