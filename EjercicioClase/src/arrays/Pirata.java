package arrays;

import java.util.Scanner;

public class Pirata {

	public static void main(String[] args) {
		/*
		 * Juego del Tesoro: Tenemos un tablero como el de la figura, de 5x5, donde hay
		 * ubicado un pirata y un tesoro. Adem�s, algunas casillas contienen agua. Se
		 * pide implementar: 
		 * a. Un m�todo para inicializar el tablero que pondr� las
		 * casillas con agua o tierra, seg�n corresponda, y colocar� al pirata y al
		 * tesoro aleatoriamente en casillas distintas. 
		 * b. Un m�todo para mostrar el
		 * tablero con el formato adecuado. 
		 * c. Un m�todo para mover al pirata en cuatro
		 * direcciones posibles. 
		 * d. Un m�todo principal que muestre el mapa y pida al
		 * usuario la direcci�n en la que se debe mover el pirata. Esto se debe repetir
		 * hasta que el pirata consiga el tesoro, caiga al agua o se agote el n�mero
		 * m�ximo de movimientos (10). 
		 * e. Se debe indicar el fin del juego y el motivo
		 */
			Scanner sc = new Scanner(System.in);
			int tablero[][] = new int[][] {
				{1,1,1,0,0},
				{1,0,0,0,0},
				{0,0,0,0,0},
				{0,0,0,0,1},
				{0,0,1,1,1}
			};
			int colPirata = 0;
			int filPirata = 0;
			boolean pirata = false;
			boolean tesoro = false;
			boolean juego = true;
			boolean muerto = false;
			boolean victoria = false;
			int intentos = 10;
			char wasd = ' ';
			String asciiTesoro = Character.toString((char)0001);
			String asciiPirata = Character.toString((char)182);
			//182
			//167

			
			
			do {
				int fila = (int) (0+(Math.random()*5));
				int columna = (int) (0+(Math.random()*5));
				if (tablero[columna][fila]!=1) {
					tablero[columna][fila] = 2;
					pirata = true;
					filPirata = fila;
					colPirata = columna;
				}
			}while(!pirata);
			
			do {
				int fila = (int) (0+(Math.random()*5));
				int columna = (int) (0+(Math.random()*5));
				if (tablero[columna][fila]!=1 && tablero[fila][columna]!=2) {
					tablero[columna][fila] = 3;
					tesoro = true;
				}
			}while(!tesoro);
			
			String dibujo[][] = new String[5][5];
			
			
			
			System.out.println("Gu�e al pirata hacia el tesoro, si cae al agua, morir�.");
			do {
				intentos--;
				for (int j = 0; j < dibujo.length; j++) {
					// Este for recorre el array m�ltiple
					for (int i = 0; i < dibujo[j].length; i++) {
						// Este array recorre los sub array.
						if (tablero[j][i]==0) {
							dibujo[j][i]="[ ]";
						}
						if (tablero[j][i]==1) {
							dibujo[j][i]="[W]";
						}
						if (tablero[j][i]==2) {
							dibujo[j][i]="["+asciiPirata+"]";
						}
						if (tablero[j][i]==3) {
							dibujo[j][i]="["+asciiTesoro+"]";
						}
					}
				}
				
			System.out.println("Tablero de juego: ");
			for (int j = 0; j < dibujo.length; j++) {
				// Este for recorre el array m�ltiple
				for (int i = 0; i < dibujo[j].length; i++) {
					// Este array recorre los sub array.
					if (i==4) {
						System.out.println(dibujo[j][i]);
					}else {
						System.out.print(dibujo[j][i]);
						//Las vac�as simplemente los corchetes.
					}
				}
			}
			System.out.println("�Hacia d�nde quiere mover al pirata? (W,A,S,D)");
			wasd = sc.next().charAt(0);
			switch (wasd) {
			case 'w':
				if (colPirata==0) {
					System.out.println("'Sorprendido, el pirata se pregunta por qu� acaba de correr directo hacia una pared.'");
					break;
				}else {
					tablero[colPirata][filPirata] = 0;
					colPirata-=1;
					if (tablero[colPirata][filPirata]==1) {
						muerto = true;
						break;
					}	
					if (tablero[colPirata][filPirata]==3) {
						victoria = true;
						break;
				}else{
						tablero [colPirata][filPirata] = 2;
					}
				break;}
			case 'a':
				if (filPirata==0) {
					System.out.println("'Sorprendido, el pirata se pregunta por qu� acaba de correr directo hacia una pared.'");
					break;
				}else {
					tablero[colPirata][filPirata] = 0;
					filPirata-=1;
					if (tablero[colPirata][filPirata]==1) {
						muerto = true;
						break;
						}
					if (tablero[colPirata][filPirata]==3) {
							victoria = true;
							break;
					}else {
					tablero [colPirata][filPirata] = 2;
					}
				break;}
			case 's':
				if (colPirata==4) {
					System.out.println("'Sorprendido, el pirata se pregunta por qu� acaba de correr directo hacia una pared.'");
					break;
				}else {
					tablero[colPirata][filPirata] = 0;
					colPirata+=1;
					if (tablero[colPirata][filPirata]==1) {
						muerto = true;
						break;
					}
					if (tablero[colPirata][filPirata]==3) {
						victoria = true;
						break;
					}else {
					tablero [colPirata][filPirata] = 2;
					}
				break;}
			case 'd':
				if (filPirata==4) {
					System.out.println("'Sorprendido, el pirata se pregunta por qu� acaba de correr directo hacia una pared.'");
					break;
				}else {
					tablero[colPirata][filPirata] = 0;
					filPirata+=1;
					if (tablero[colPirata][filPirata]==1) {
						muerto = true;
						break;
					}
					if (tablero[colPirata][filPirata]==3) {
						victoria = true;
						break;
					}else {
					tablero [colPirata][filPirata] = 2;
					}
				break;}
			default:
				System.out.println("Introduzca una direcci�n v�lida.");
				break;
			}
			if (muerto) {
				juego = false;
			}if (victoria) {
				juego = false;
			}if (intentos == 0) {
				juego = false;
			}
			}while(juego);	
			sc.close();
			if (muerto) {
			System.out.println("'Con un sonido de chapoteo, el pirata cay� al agua, minutos mas tarde, se ahog�.'\nGAME OVER.");
			}if (victoria) {
				System.out.println("'Con un crujido seco, el cofre se abre y revela un tesoro de incontables monedas... Que r�pidamente se convirtieron en ron. Suerte buscando el pr�ximo tesoro.'\nVICTORIA.");
			}else if (intentos == 0) {
				System.out.println("'Tras horas de infructuosa b�squeda, el pirata se rindi� sin conseguir su preciado premio.'\nGAME OVER.");
			}
			}
}
