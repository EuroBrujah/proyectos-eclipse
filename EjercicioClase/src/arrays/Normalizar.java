package arrays;

import java.util.Scanner;

public class Normalizar {

	public static void main(String[] args) {
		// Implemente un programa para normalizar un array.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		double n[] = new double[5];
		double n2[] = new double[5];
		//Inicializamos 2 array con 5 posiciones.
		double mayor = 0;
		//Variable para almacenar el mayor entre las posiciones del array 1.
		double menor = 345;
		//Variable para almacenar el menor entre las posiciones del array 1.
		System.out.println("Introduzca 5 n�meros reales: ");
		for (int i = 0; i < n.length; i++) {
			n[i] = sc.nextDouble();
			// For que recorre las posiciones del array y las va rellenando con input del
			// usuario.
		}
		for (int i = 0; i < n.length; i++) {
			if (n[i] > mayor) {
				mayor = n[i];
			}
			if (n[i] < menor) {
				menor = n[i];
				// For que recorre las posiciones del array y va rellenando el mayor y el menor.
			}
		}

		for (int i = 0; i < n.length; i++) {
			n2[i] = (n[i] - menor) / (mayor - menor);
			//For que almacena la normalizaci�n de cada posici�n del array 1 en el segundo array.
		}
		for (int i = 0; i < n2.length; i++) {
			System.out.println(n2[i]);
			//For para leer los resultados del array 2.
		}
		sc.close();
		//Cerramos teclado.
	}

}
