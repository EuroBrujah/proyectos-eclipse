package arrays;

import java.util.Scanner;

public class Cine {

	public static void main(String[] args) {
		// Crea un cine con 14 filas y 22 columnas en el que si el usuario intenta
		// reservar un asiento le diga si est�, o no, reservado.
		Scanner sc = new Scanner(System.in);
		boolean butaca[][] = new boolean[14][22];
		String dibujo[][] = new String[14][22];
		//Inicializamos un array de booleanos para indicar si las butacas est�n en false (vac�as) o en true (ocupadas)
		char salir = 's';
		//Variable para almacenar condici�n de salida.
		
		//boolean reservado = false;
		//boolean pasillo = false;
		//boolean ocupada = false;
		//int reservaButaca = 0;
		//int reservaFila = 0;
		
		do {
			//Bucle do para realizar el proceso de reserva hasta que le indiquemos que salga.
			System.out.println("Introduzca la fila en la que quiere sentarse (Filas 1 a 14): ");
			int fila = sc.nextInt()-1;
			//Solicitamos al usuario la fila y la almacenamos.
			System.out.println("A continuaci�n introduzca su asiento (Asientos 1 a 22): ");
			int asiento = sc.nextInt()-1;
			//Solicitamos al ususario el asiento y lo almacenamos.
			if(asiento==10 || asiento==11) {
				System.out.println("Esa posici�n corresponde al pasillo, disculpe las molestias.");
				//Si corresponde a las posiciones 10 u 11, devolvemos este mensaje.
			}
			else if(butaca[fila][asiento]==true) {
				System.out.println("Esa butaca est� ocupada, disculpe las molestias.");
				//Si el bool estuviese en true, devolvemos este mensaje.
				
			}else {
				butaca[fila][asiento] = true;
				System.out.println("Ha reservado el asiento " + (asiento + 1) + " de la fila " + (fila + 1) + ".");
				//En caso contrario, reservamos la butaca, poniendo el flag en true e imprimimos este mensaje.
				
			}
			/* for (int j = butaca.length - 1; j >= fila - 1; j--) {
				// Este for recorre el array m�ltiple
				for (int i = butaca[j].length - 1; i >= asiento - 1; i--) {
					// Este array recorre los sub array.
					if (i == 10 || i == 11) {
						butaca[j][i] = true;
						pasillo = true;
					}
					if (j == fila - 1 && i == asiento - 1 && !butaca[j][i]) {
						butaca[j][i] = true;
						reservaFila = j;
						reservaButaca = i;
						reservado = true;
					}
					if (j == fila - 1 && i == asiento - 1 && butaca[j][i]) {
						ocupada = true;
					}
				}
			}
			if (reservado) {
				System.out.println(
						"Ha reservado el asiento " + (reservaButaca + 1) + " de la fila " + (reservaFila + 1) + ".");
			} else if (!reservado && pasillo) {
				System.out.println("Esa posici�n corresponde al pasillo, disculpe las molestias.");
			} else if (!reservado && ocupada) {
				System.out.println("Esa butaca est� ocupada, disculpe las molestias.");
			}
			reservado = false;
			pasillo = false;
			ocupada = false;
			reservaButaca = 0;
			reservaFila = 0;*/
			//System.out.println(Arrays.deepToString(butaca));
			
			for (int j = 0; j < dibujo.length; j++) {
				// Este for recorre el array m�ltiple
				for (int i = 0; i < dibujo[j].length; i++) {
					// Este array recorre los sub array.
					if (i==10 || i==11) {
						dibujo[j][i] = "[P]";
						//En el pasillo pinta P.
					}
					else if (butaca[j][i]) {
						dibujo[j][i] = "[*]";
						//Las reservadas son un asterisco.
						
					}else {
						dibujo [j][i] = "[ ]";
						//Las vac�as simplemente los corchetes.
					}
				}
			}
			
			for (int j = 0; j < dibujo.length; j++) {
				// Este for recorre el array m�ltiple
				for (int i = 0; i < dibujo[j].length; i++) {
					// Este array recorre los sub array.
					if (i==21) {
						System.out.println(dibujo[j][i]);
						//Imprimimos los saltos de l�nea al final de cada fila.
					}
					else {
						System.out.print(dibujo[j][i]);
					}
				}
			}
			System.out.println("�Desea comprar m�s entradas? (s/n)");
			salir = sc.next().charAt(0);
			//Introducimos si queremos o no salir.

		} while (salir != 'n');
		sc.close();
		//Cerramos teclado.
	}

}
