package arrays;

import java.util.Scanner;

public class Vectores {

	public static void main(String[] args) {
		// Crear dos arrays de tres elementos, cada uno de ellos representará un vector
		// de tres dimensiones (x, y, z).
		// Solicitar al usuario los datos para ambos vectores.
		// Mostrarlos en la forma.
		// Calcular su producto escalar.
		Scanner sc = new Scanner(System.in);

		int v1[] = new int[3];
		int v2[] = new int[3];
		int esc[] = new int[3];

		System.out.println("Introduzca las posiciones x, y, z del primer vector: ");
		for (int i = 0; i < v1.length; i++) {
			v1[i] = sc.nextInt();
			// For que recorre las posiciones del array y las va rellenando con input del
			// usuario.

		}
		System.out.println("Introduzca las posiciones x, y, z del segundo vector: ");
		for (int i = 0; i < v2.length; i++) {
			v2[i] = sc.nextInt();
			// For que recorre las posiciones del array y las va rellenando con input del
			// usuario.

		}
		System.out.println("El primer vector es " + v1[0] + ", " + v1[1] + ", " + v1[2] + "\nEl segundo vector es "
				+ v2[0] + ", " + v2[1] + ", " + v2[2] + ".");
		esc[0] = (v1[0] * v2[0]);
		esc[1] = (v1[1] * v2[1]);
		esc[2] = (v1[2] * v2[2]);
		System.out.println("El factor escalar es " + esc[0] + ", " + esc[1] + ", " + esc[2] + ".");
		sc.close();
	}
}
