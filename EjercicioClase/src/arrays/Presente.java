package arrays;

import java.util.Scanner;

public class Presente {

	public static void main(String[] args) {
		// Introducir un n�mero y decir si est� en el array o no.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int n[] = new int[] {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
		//Inicializamos un array.
		boolean bool = false;
		//Iniciamos un booleano que almacenar� si el n�mero ha sido encontrado, por defecto, no.
		int comprobar = 0;
		//Variable en la que almacenaremos el n�mero que introduzca el usuario.
		
		System.out.println("Introduzca un n�mero para saber si est� en el array: ");
		comprobar = sc.nextInt();
		//Solicitamos al usuario un n�mero y lo almacenamos.
		for (int i=0; i<n.length;i++) {
			if (comprobar==n[i]) {
				bool=true;
				//Este for recorre el array y si el n�mero introducido est� presente cambia el bool a true.
			}
		}
		if (bool) {
			System.out.println("El n�mero "+comprobar+" est� inclu�do en el array.");
		}else {
			System.out.println("El n�mero "+comprobar+" no est� inclu�do en el array.");
			//Indica si el n�mero ha sido o no encontrado al usuario.
		}
		sc.close();
		//Cerramos teclado.
		
	}

}
