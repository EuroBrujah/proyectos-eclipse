package arrays;

public class Notas {

	public static void main(String[] args) {
		// Implementar un programa que calcule la media de 6, 5.1, 7.25, 5.3, 9.2, 3.1 y 9.

		double n[] = new double[] { 6, 5.1, 7.25, 5.3, 9.2, 3.1, 9 };
		//Inicializamos un array con las notas.
		double suma = 0;
		double media = 0;
		//Inicializamos una variable para la suma y para la media.

		for (int i = 0; i < n.length; i++) {
			suma += n[i];
			//Este for recorre todas las posiciones del array y suma todo a la variable suma.
		}
		media = (double) Math.round((suma / n.length)*100)/100 ;
		//Calculamos la media dividiendo la suma por el largo del array y redondeamos.
		
		System.out.println("La media de sus notas es "+media);
		//Imprimimos el resultado.

	}

}
