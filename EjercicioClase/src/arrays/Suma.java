package arrays;

import java.util.Arrays;
import java.util.Scanner;

public class Suma {

	public static void main(String[] args) {
		// Pedir 2 array multi al usuario y sumarlos.

		Scanner sc = new Scanner(System.in);

		int mArray1[][] = new int[2][3];
		int mArray2[][] = new int[2][3];
		int mArray3[][] = new int[2][3];

		for (int j = 0; j < mArray1.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray1[j].length; i++) {
				// Este array recorre los sub array.
				System.out.println("Introduzca un n�mero para la posici�n " + (i + 1) + " del array " + (j + 1) + ".");
				mArray1[j][i] = sc.nextInt();
				// Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del
				// array m�ltiple (j) y del sub array (i) el n�mero.
			}
		}
		for (int j = 0; j < mArray2.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray2[j].length; i++) {
				// Este array recorre los sub array.
				System.out.println("Introduzca un n�mero para la posici�n " + (i + 1) + " del array " + (j + 1) + ".");
				mArray2[j][i] = sc.nextInt();
				// Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del
				// array m�ltiple (j) y del sub array (i) el n�mero.

			}

		}
		for (int j = 0; j < mArray3.length; j++) {
			// Este for recorre el array m�ltiple
			for (int i = 0; i < mArray3[j].length; i++) {
				// Este array recorre los sub array.
				mArray3[j][i] = mArray1[j][i] + mArray2[j][i];
				// Solicitamos al usuario el n�mero por teclado y almacenamos en la posici�n del
				// array m�ltiple (j) y del sub array (i) el n�mero.
			}
		}
		System.out.println(Arrays.deepToString(mArray3));
		//Imprimimos el resultado.
				sc.close();
				//Cerramos teclado.
			}
	}

