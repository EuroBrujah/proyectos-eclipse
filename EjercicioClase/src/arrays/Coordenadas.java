package arrays;

import java.util.Arrays;

public class Coordenadas {

	public static void main(String[] args) {
		// Crea un array de 3x3 elementos y rellena sus valores de manera aleatoria. Despu�s 
		// busca y muestra las coordenadas del elemento con mayor y menor valor.
		int matriz[][] = new int[3][3];
		int coordMayor[] = new int[2];
		int coordMenor[] = new int[2];
		//Declaramos una matriz de 3x3 y dos array en los que almacenar las coordenadas del mayor y el menor.
		int mayor = -1;
		int menor = 10;
		//Inicializamos dos variables para comprobar mayor y menor.
		
		
		for (int i=0; i<matriz.length;i++) {
			for (int j=0; j<matriz.length;j++) {
				matriz[i][j] = (int) (0+Math.random()*10);
			}
		}
		//Este for recorre la matriz y asigna valores aleatorios de 0 a 9.
		
		for (int i=0; i<matriz.length;i++) {
			//Este for recorre el array principal.
			for (int j=0; j<matriz.length;j++) {
				//Este for recorre los sub array.
				if (matriz[i][j]>mayor) {
					mayor = matriz[i][j];
					coordMayor[0] = i;
					coordMayor[1] = j;
					//Si la posici�n es mayor al mayor, almacena la variable y su posici�n.
				}
				if (matriz[i][j]<menor) {
					menor = matriz[i][j];
					coordMenor[0] = i;
					coordMenor[1] = j;
					//Si la posici�n es menor al menor, almacena la variable y su posici�n.
				}
			} 
		}
		System.out.println("Matriz: "+Arrays.deepToString(matriz));
		System.out.println("El n�mero menor de la matriz es el "+menor+" y sus coordenadas son "+Arrays.toString(coordMenor)+".");
		System.out.println("El n�mero mayor de la matriz es el "+mayor+" y sus coordenadas son "+Arrays.toString(coordMayor)+".");
		//Imprimimos la matriz y los resultados de la b�squeda.
	}

}
