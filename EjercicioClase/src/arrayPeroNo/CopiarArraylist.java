package arrayPeroNo;

import java.util.ArrayList;
import java.util.List;

public class CopiarArraylist {

	public static void main(String[] args) {
		// Copiamos un arraylist en otro.
		List<String> lista1 = new ArrayList<String>();
		List<String> lista2 = new ArrayList<String>();
		//Inicializamos ambos arrayList.
		
		lista1.add("Hola");
		lista1.add("Adios");
		//Metemos algo en el primero.
		
		for (int i = 0; i<lista1.size();i++) {
			lista2.add(lista1.get(i));
			//Este for recorre el primer array, obtiene el valor de la iteraci�n actual y lo a�ade al segundo.
		}
		
		
		System.out.println("Lista 1: "+lista1);
		System.out.println("Lista 2: "+lista2);
		//Imprimimos ambos para comparar.
	}

}
