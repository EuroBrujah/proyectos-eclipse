package arrayPeroNo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SubArrayList {

	public static void main(String[] args) {
		// El usuario introduce dos posiciones del arrayList y generamos un arraylist que contenga los elementos desde la primera posición hasta la segunda.
		List<Integer> lista1 = new ArrayList<Integer>();
		List<Integer> lista2 = new ArrayList<Integer>();
		//Inicializamos los dos arrayList.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		
		for (int i=0;i<8;i++) {
			lista1.add(i);
			//Rellenamos el array.
		}
		System.out.println("Lista 1: "+lista1);
		//Imprimimos el array.
		
		System.out.println("Introduzca la posición por la que quiere empezar la sublista: ("+lista1.size()+" posiciones).");
		int comienzo = sc.nextInt();
		//Solicitamos al usuario la posición inicial y la almacenamos.
		System.out.println("Ahora introduzca la posición en la que quiere terminar la sublista.");
		int fin = sc.nextInt();
		//Solicitamos al usuario la posición final y la almacenamos.
		for (int i=comienzo;i<=fin;i++) {
			lista2.add(lista1.get(i));
			//Empezamos el bucle en la variable 1 y lo finalizamos en la variable 2. En cada iteración almacenamos el valor correspondiente en el arrayList 2.
		}
		System.out.println("Lista 2: "+lista2);
		//Imprimimos resultados.
		
	sc.close();
	//Cerramos teclado.
	}
}
