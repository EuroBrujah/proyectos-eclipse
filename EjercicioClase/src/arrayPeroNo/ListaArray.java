package arrayPeroNo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListaArray {

	public static void main(String[] args) {
		// Declaraci�n de ArrayList
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca un valor para introducirlo a la lista: ");
		String valor = sc.next();
		List<String> arrayColores= new ArrayList<String>();
		arrayColores.add("Rojo");
		arrayColores.add("Asulito sielo");
		arrayColores.add(1, "Verde");
		arrayColores.add(valor);
		
		System.out.println(arrayColores);
		arrayColores.set(3, "Morado");
		System.out.println(arrayColores);
		System.out.println("El valor en la posici�n 3 es "+arrayColores.get(3)+".");
		arrayColores.remove(3);
		System.out.println(arrayColores);
		
		
		sc.close();
	}

}
