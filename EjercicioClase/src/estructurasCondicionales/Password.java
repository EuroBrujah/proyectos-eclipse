package estructurasCondicionales;

import java.util.Scanner;

public class Password {

	public static void main(String[] args) {
		// Introducimos una contrase�a por teclado y despu�s comprueba si es correcta o
		// incorrecta
		Scanner sc = new Scanner(System.in);
		String password = "123456";
		//Inicializamos Scanner y definimos la contrase�a

		System.out.println("Introduzca su contrase�a: ");
		String pass = sc.next();
		//Solicitamos la contrase�a al usuario y la almacenamos en la variable
		if (pass.equals(password)) {
			//System.out.println("La contrase�a introducida no es correcta. Int�ntelo de nuevo.");
			System.out.println("Contrase�a correcta. Bienvenido.");
		} else {
			//System.out.println("Contrase�a correcta. Bienvenido.");
			System.out.println("La contrase�a introducida no es correcta. Int�ntelo de nuevo.");
		}
		//Si la contrase�a es la definida, le dejamos acceder al programa, si no, no.
		sc.close();
		//Cerramos teclado
	}
}
