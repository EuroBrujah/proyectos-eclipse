package estructurasCondicionales;

import java.util.Scanner;

public class IfElse {

	public static void main(String[] args) {
		// Leer dos n�meros por teclado y decir cual es mayor.
		Scanner sc = new Scanner(System.in);
		// Inicializamos teclado.

		System.out.println("Introduzca un n�mero: ");
		int n1 = sc.nextInt();
		// Solicitamos el primer n�mero y lo almacenamos en una variable.
		System.out.println("Introduzca un n�mero para compararlo con el primero: ");
		int n2 = sc.nextInt();
		// Solicitamos el segundo n�mero y lo almacenamos.

		if (n1 >= n2) // Comprobamos si el primero es mayor o igual que el segundo.
		{
			System.out.println("El n�mero " + n1 + " es mayor o igual que el segundo n�mero.");
		} else {
			System.out.println("El n�mero " + n2 + " es mayor que el primero.");
		} // Si el primero es mayor o igual, imprimimos el primer resultado, si el segundo
			// es
			// mayor, imprimimos el segundo.
		sc.close();
		// Cerramos teclado.
	}

}
