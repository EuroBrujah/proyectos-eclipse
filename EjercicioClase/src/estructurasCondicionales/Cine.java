package estructurasCondicionales;

import java.util.Scanner;

public class Cine {

	public static void main(String[] args) {
		// Programa que pide edad del usuario y d�a de la semana y le calcula el precio de la entrada de cine
		Scanner sc = new Scanner(System.in);
		final int ENTRADA = 15;
		final double DESCUENTO60 = 0.2;
		final double DESCUENTO4 = 0.6;
		final double DESCUENTO12 = 0.4;
		final double DESCUENTOMIERC = 0.5;
		
		System.out.println("Introduzca su edad: ");
		int edad = sc.nextInt();
		System.out.println("Introduzca el d�a de la semana (1-7): ");
		int dia = sc.nextInt();
		
		if (dia==3) {
			if (edad <=4) {
				System.out.println("El precio de su entrada es "+((ENTRADA*DESCUENTOMIERC)*DESCUENTO4)+" euros. Disfrute de la pel�cula.");
			}else if (edad >4&& edad <=12) {
				System.out.println("El precio de su entrada es "+((ENTRADA*DESCUENTOMIERC)*DESCUENTO12)+" euros. Disfrute de la pel�cula.");
			}else if (edad >60) {
				System.out.println("El precio de su entrada es "+((ENTRADA*DESCUENTOMIERC)*DESCUENTO60)+" euros. Disfrute de la pel�cula.");
			}else {
				System.out.println("El precio de su entrada es "+(ENTRADA*DESCUENTOMIERC)+" euros. Disfrute de la pel�cula.");
			}
			
		}else if (dia <= 7 && dia !=3){
			if (edad <=4) {
				System.out.println("El precio de su entrada es "+(ENTRADA*DESCUENTO4)+" euros. Disfrute de la pel�cula.");
			}else if (edad >4&& edad <=12) {
				System.out.println("El precio de su entrada es "+(ENTRADA*DESCUENTO12)+" euros. Disfrute de la pel�cula.");
			}else if (edad >60) {
				System.out.println("El precio de su entrada es "+(ENTRADA*DESCUENTO60)+" euros. Disfrute de la pel�cula.");
			}else {
				System.out.println("El precio de su entrada es "+ENTRADA+" euros. Disfrute de la pel�cula.");
			}
		}else {
	System.out.println("Introduzca un d�a de la semana v�lido");
}sc.close();
	}
}
