package estructurasCondicionales;

import java.util.Scanner;

public class Meses {

	public static void main(String[] args) {
		//Pide un n�mero por teclado y te devuelve el mes correspondiente
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado
		
		System.out.println("Introduzca un n�mero del 1 al 12 para conocer el mes al que corresponde");
		int mes=sc.nextInt();
		//Solicitamos un n�mero del 1 al 12 y almacenamos la variable
		
		switch (mes) {
		case 1: 
			System.out.println("El mes correspondiente al "+mes+" es enero.");
			break;
		case 2:
			System.out.println("El mes correspondiente al "+mes+" es febrero.");
			break;
		case 3:
			System.out.println("El mes correspondiente al "+mes+" es marzo.");
			break;
		case 4:
			System.out.println("El mes correspondiente al "+mes+" es abril.");
			break;
		case 5:
			System.out.println("El mes correspondiente al "+mes+" es mayo.");
			break;
		case 6:
			System.out.println("El mes correspondiente al "+mes+" es junio.");
			break;
		case 7:
			System.out.println("El mes correspondiente al "+mes+" es julio.");
			break;
		case 8:
			System.out.println("El mes correspondiente al "+mes+" es agosto.");
			break;
		case 9:
			System.out.println("El mes correspondiente al "+mes+" es septiembre.");
			break;
		case 10:
			System.out.println("El mes correspondiente al "+mes+" es octubre.");
			break;
		case 11:
			System.out.println("El mes correspondiente al "+mes+" es noviembre.");
			break;
		case 12:
			System.out.println("El mes correspondiente al "+mes+" es diciembre.");
			break;
		default:
			System.out.println("Introduzca un n�mero v�lido.");
			//Montamos un switch donde cada caso es uno de los 12 n�meros posibles, cada caso imprime el mes correspondiente al n�mero
			//el default simplemente solicita un nuevo n�mero v�lido en caso de que no se cumpla ninguno de los casos.
		}
		sc.close();
		//Cerramos teclado
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
