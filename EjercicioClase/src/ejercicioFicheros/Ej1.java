package ejercicioFicheros;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		// Implemente un programa que solicite el nombre de un archivo, a continuación lea su contenido y lo muestre por pantalla.
		Scanner sc = new Scanner(System.in);
		//Inicializamos Scanner
		System.out.println("Introduzca la ruta completa del archivo que desea leer, recuerde incluir la extensión del archivo. ");
		String docu = sc.next();
		//Solicitamos al usuario la ruta del archivo y lo almacenamos en un String.
		try {
			File ficherolectura = new File (docu);
			//Accedemos al fichero que coincida con el nombre de la variable docu.
			Scanner sc2 = new Scanner(ficherolectura);
			//Leemos su contenido con el Scanner
			while(sc2.hasNextLine()) {
				//Este bucle hace que mientras siga habiendo texto, se siga repitiendo.
			String datos = sc2.next();
			//Almacenamos el contenido del Scanner.
			System.out.print(datos + " ");
			//Imprimimos los resultados.
			}
			sc2.close();
		}catch(IOException e){
			System.out.println("Error de lectura: "+e);
			//Catch para tratamiento de excepciones.
			
		}
		sc.close();
		//Cerramos teclado.
	}

}
