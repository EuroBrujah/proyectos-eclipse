package ejercicioFicheros;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ej2 {

	public static void main(String[] args) {
		/*
		 * Implemente un HOWLER, es decir, un programa que lea un mensaje introducido
		 * por teclado y lo escriba en un archivo EN MAY�SCULAS. El programa ir� leyendo
		 * l�nea a l�nea y guardando en el archivo. Debe parar cuando se introduzca una
		 * l�nea vac�a.
		 */

		Scanner sc = new Scanner(System.in);
		//Inicializamos Scanner.
		String mensaje;
		String auxiliar = "";
		//Creamos dos String para almacenar los mensajes.
		File howler = new File("C:\\Users\\DAM\\Desktop\\Ficheros\\Howler.txt");
		//Creamos el howler.
		
		do {
			System.out.println("\nIntroduzca una frase para introducirla en el HOWLER. Pulsa intro para salir.");
			mensaje = sc.nextLine();
			//Este buble se repite mientras el usuario meta algo distinto a intro.

			if (!mensaje.equals("")) {
				//Si el usuario no introduce nada, saltamos directos a la condici�n de salida.
			FileWriter escritura;
			try {
				escritura = new FileWriter("C:\\Users\\DAM\\Desktop\\Ficheros\\Howler");
				//Inicializamos un filewriter linkeado al archivo reci�n creado.
				escritura.write(mensaje.toUpperCase());
				//Volcamos el mensaje en may�sculas.
				escritura.close();
			} catch (IOException error) {

			}
			try {
				File ficherolectura = new File("C:\\Users\\DAM\\Desktop\\Ficheros\\Howler");
				Scanner sc2 = new Scanner(ficherolectura);
				//Inicializamos un filewriter y un scanner que lo use.
				while (sc2.hasNextLine()) {
					String datos = sc2.next();
					//Almacenamos los datos que lee.
					auxiliar += "\n" + datos;
					//Volcamos los datos en el auxiliar.
					System.out.print("Howler: \n" + auxiliar);
					//Imprimimos los resultados.
				}
				sc2.close();
			} catch (IOException e) {
				System.out.println("Error de lectura: " + e);

			}
			
			try {
				escritura = new FileWriter("C:\\Users\\DAM\\Desktop\\Ficheros\\Howler");
				//Inicializamos un filewriter linkeado al archivo reci�n creado.
				escritura.write(auxiliar.toUpperCase());
				//Volcamos el mensaje en may�sculas.
				escritura.close();
			} catch (IOException error) {

			}
			}
		} while (!mensaje.equals(""));
		//Sale si es intro.
		sc.close();
		//Cerramos Scanner.
	}

}
