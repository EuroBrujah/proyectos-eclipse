package ejercicioFicheros;

import java.io.File;
import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		// Escriba un programa que, partiendo de un directorio dado, muestre todos los
		// archivos con una extensi�n determinada.
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca la extensi�n de los ficheros a buscar: ");
		String extension = sc.next();

		File directorio = new File("C:\\Users\\DAM\\Desktop");
		String[] ArraydeFichero = directorio.list();
		for (String name : ArraydeFichero) {
			if (name.contains(extension)) {
				System.out.println(name);
			}
		}
	sc.close();
	}
}
