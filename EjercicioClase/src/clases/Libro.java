package clases;

public class Libro {
	/*
	 * ISBN titulo autor nPaginas genero a�oPubli editorial tipoLibro disponibleSN
	 * m�todo pr�stamo
	 * m�todo comprobar nPaginas libro
	 * sobrecargar toString para que imprima la info del libro estilo arrayList
	 */
	
	private String isbn;
	private String titulo;
	private String autor;
	private int nPaginas;
	private String genero;
	private int a�oPubli;
	private String editorial;
	private String tipoLibro;
	private boolean disponible;
	private String disp = "Disponible";
	
	public Libro () {
		
	}
	public Libro (String isbn, String titulo, String autor, int nPaginas, String genero, int a�oPubli, String editorial, String tipoLibro, boolean disponible) {
		this.isbn=isbn;
		this.titulo=titulo;
		this.autor=autor;
		this.nPaginas=nPaginas;
		this.genero=genero;
		this.a�oPubli=a�oPubli;
		this.editorial=editorial;
		this.tipoLibro=tipoLibro;
		this.disponible=disponible;
		if(!disponible) {
		disp="No disponible";
				}		
	}
	
	public void prestamo() {
		if(disponible) {
			disponible=false;
			disp="No disponible";
			System.out.println("Ha solicitado el pr�stamo del libro.");
		}else {
			System.out.println("Lo sentimos, el libro se encuentra actualmente en pr�stamo.");
		}
	}
	public void devolver() {
		if(!disponible) {
			disponible=true;
			disp="Disponible";
			System.out.println("Ha devuelto "+this.titulo+".");
		}else {
			System.out.println("Este libro ya se encuentra en la biblioteca.");
		}
	}
	public void compararPag(Libro libro2) {
		if(this.nPaginas>libro2.nPaginas) {
			System.out.println(this.titulo+" tiene m�s p�ginas que "+libro2.titulo+".");
			}else if (this.nPaginas<libro2.nPaginas) {
				System.out.println(libro2.titulo+" tiene m�s p�ginas que "+this.titulo+".");
			}else {
				System.out.println("Ambos libros tienen el mismo n�mero de p�ginas");
			}
		}
	@Override
	public String toString() {
	return "T�tulo: "+this.titulo+".\nAutor: "+this.autor+".\nISBN: "+this.isbn+".\nN�mero de p�ginas: "+this.nPaginas+".\nG�nero: "+this.genero+".\nA�o de publicaci�n: "+this.a�oPubli+".\nEditorial: "+this.editorial+".\nTipo de Libro: "+this.tipoLibro+".\nDisponibilidad: "+this.disp+".\n";	
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public int getnPaginas() {
		return nPaginas;
	}
	public void setnPaginas(int nPaginas) {
		this.nPaginas = nPaginas;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public int getA�oPubli() {
		return a�oPubli;
	}
	public void setA�oPubli(int a�oPubli) {
		this.a�oPubli = a�oPubli;
	}
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	public String getTipoLibro() {
		return tipoLibro;
	}
	public void setTipoLibro(String tipoLibro) {
		this.tipoLibro = tipoLibro;
	}
	public boolean isDisponible() {
		return disponible;
	}
	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}
	public String getDisp() {
		return disp;
	}
	public void setDisp(String disp) {
		this.disp = disp;
	}
}
