package clases;

public class Persona {
	
	private String nombre;
	private String apellidos;
	private String dni;
	private int edad;
	private boolean casado;
	
	private int nBrazos=2;
	private int nPiernas=2;
	
	public Persona(String nombre, String apellidos, String dni, int edad) {
		this.setNombre(nombre);
		this.setApellidos(apellidos);
		this.setDni(dni);
		this.setEdad(edad);
	}
	
	public Persona(String nombre, String apellidos, String dni, int edad, int nBrazos, int nPiernas) {
		this.setNombre(nombre);
		this.setApellidos(apellidos);
		this.setDni(dni);
		this.setEdad(edad);
		this.setnBrazos(nBrazos);
		this.setnPiernas(nPiernas);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getnBrazos() {
		return nBrazos;
	}

	public void setnBrazos(int nBrazos) {
		this.nBrazos = nBrazos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getnPiernas() {
		return nPiernas;
	}

	public void setnPiernas(int nPiernas) {
		this.nPiernas = nPiernas;
	}

	public boolean isCasado() {
		return casado;
	}

	public void setCasado(boolean casado) {
		this.casado = casado;
	}

}
