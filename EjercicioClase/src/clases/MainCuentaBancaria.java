package clases;

import java.util.Scanner;

public class MainCuentaBancaria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		CuentaBancaria cuenta1 = new CuentaBancaria ("Javier Naranjo");
		CuentaBancaria cuenta2 = new CuentaBancaria ("Enrique Casanova", 13589.68);
		boolean salir=false;
		int menu = 0;
		
		do {
			System.out.println("Bienvenido a la aplicaci�n de BBVA:\n�Qu� desea hacer?\n1.Consultar datos de sus cuentas.\n2.Realizar ingreso.\n3.Realizar retirada.\n4.Realizar transferencia.");
			menu = sc.nextInt();
			switch (menu) {
			case 1:
				System.out.println("�Que cuenta quiere consultar?\n1."+cuenta1.getTitular()+".\n2."+cuenta2.getTitular()+".");
				menu = sc.nextInt();
				if (menu==1) {
					System.out.println(cuenta1.toString());
				}else if (menu==2) {
					System.out.println(cuenta2.toString());
				}else {
					System.out.println("Introduzca una de las opciones v�lidas.");
				}
				break;
			case 2:
				System.out.println("�En que cuenta quiere realizar la operaci�n?\n1."+cuenta1.getTitular()+".\n2."+cuenta2.getTitular()+".");
				menu = sc.nextInt();
				if (menu==1) {
					System.out.println("Introduzca la cantidad.");
					cuenta1.ingresar(sc.nextDouble());
				}else if (menu==2) {
					System.out.println("Introduzca la cantidad.");
					cuenta2.ingresar(sc.nextDouble());
				}else {
					System.out.println("Introduzca una de las opciones v�lidas.");
				}
				break;
			case 3:
				System.out.println("�En que cuenta quiere realizar la operaci�n?\n1."+cuenta1.getTitular()+".\n2."+cuenta2.getTitular()+".");
				menu = sc.nextInt();
				if (menu==1) {
					System.out.println("Introduzca la cantidad.");
					cuenta1.retirar(sc.nextDouble());
				}else if (menu==2) {
					System.out.println("Introduzca la cantidad.");
					cuenta2.retirar(sc.nextDouble());
				}else {
					System.out.println("Introduzca una de las opciones v�lidas.");
				}
				break;
			case 4:
				System.out.println("�En que cuenta quiere realizar la operaci�n?\n1."+cuenta1.getTitular()+".\n2."+cuenta2.getTitular()+".");
				menu = sc.nextInt();
				if (menu==1) {
					System.out.println("Introduzca la cantidad.");
					cuenta1.transferencia(cuenta2, sc.nextDouble());
				}else if (menu==2) {
					System.out.println("Introduzca la cantidad.");
					cuenta2.transferencia(cuenta1, sc.nextDouble());
				}else {
					System.out.println("Introduzca una de las opciones v�lidas.");
				}
				break;
			default:
				System.out.println("Introduzca una de las opciones v�lidas.");
				break;
			}

			System.out.println("�Desea realizar alguna otro tr�mite?");
			String resp=sc.next();
			if(!resp.equalsIgnoreCase("si")) {
				salir=true;
			}
		}while(!salir);
	sc.close();
	}

}
