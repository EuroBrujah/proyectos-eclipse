package clases;

import java.util.ArrayList;

public class Jugador {
	private int idJugador;
	private ArrayList<Carta> mano;
	private int valorMano;
	private boolean plantarse;

	
	public Jugador() {
		
	}
	public Jugador(int idJugador) {
		this.idJugador=idJugador;
		this.mano=new ArrayList<Carta>();
		plantarse = false;
	}
	public void robar(Mazo mazo) {
		Carta cartaRobada;
		if(!plantarse) {
		cartaRobada=mazo.sacarCarta();
		mano.add(cartaRobada);
		this.valorMano+=cartaRobada.getValor();
		mazo.eliminarCarta();
		}else {
			System.out.println("El jugador "+idJugador+" se ha plantado y no puede robar m�s cartas.");
		}
		
	}
	public int comprobarMano() {
		return valorMano;
	}
	public void plantarse() {
		this.plantarse=true;
	}
	public int getIdJugador() {
		return idJugador;
	}
	public void setIdJugador(int idJugador) {
		this.idJugador = idJugador;
	}
	public ArrayList<Carta> getMano() {
		return mano;
	}
	public void setMano(ArrayList<Carta> mano) {
		this.mano = mano;
	}
	public int getValorMano() {
		return valorMano;
	}
	public void setValorMano(int valorMano) {
		this.valorMano = valorMano;
	}
	public boolean isPlantarse() {
		return plantarse;
	}
	public void setPlantarse(boolean plantarse) {
		this.plantarse = plantarse;
	}
	
}
