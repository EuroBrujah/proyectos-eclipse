package clases;

public class Punto {
	private double x;
	private double y;
	
	public Punto() {
		this.x=0;
		this.y=0;
	}
	
	public Punto (double x, double y) {
		this.x=x;
		this.y=y;
	}
	
	public void desX(double mod) {
		this.x+=mod;
	}
	public void desY(double mod) {
		this.y+=mod;
	 }
	public void desXY(double mod) {
		this.x+=mod;
		this.y+=mod;
	 }

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "("+this.x+","+this.y+")";
	
}
}