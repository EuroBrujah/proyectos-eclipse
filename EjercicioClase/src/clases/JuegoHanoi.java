package clases;

import java.util.ArrayList;

public class JuegoHanoi {
	private ArrayList<PilaOrdenada> torres;
	private int nDiscos;
	private boolean terminado;
	
	//Constructores
	public JuegoHanoi() {
		this.torres=new ArrayList<PilaOrdenada>();
		this.nDiscos=0;
		this.terminado=false;
	}
	public JuegoHanoi(int nDiscos) {
		this.torres=new ArrayList<PilaOrdenada>();
		this.torres.add(new PilaOrdenada());
		this.torres.add(new PilaOrdenada());
		this.torres.add(new PilaOrdenada());
		this.nDiscos=nDiscos;
		this.terminado = false;
		for (int i=this.nDiscos;i>0;i--) {
			this.torres.get(0).meterDisco(new Disco(i));
		}
	}
	//Getters y Setters
	public ArrayList<PilaOrdenada> getTorres() {
		return torres;
	}
	public int getnDiscos() {
		return nDiscos;
	}
	public boolean isTerminado() {
		return terminado;
	}
	public void setTerminado(boolean terminado) {
		this.terminado = terminado;
	}
	
	//M�todos
	@Override
	public String toString() {
		String res="";
		for (int i=0;i<this.torres.size();i++) {
			res+="Torre " +(i+1)+"\n"+this.torres.get(i) + "\n";
		}
		res+="==============";
		return res;
	}
	public boolean mover(int origen, int destino){
		boolean result = false;
		if(this.torres.get(origen).checkTop().getTama�o()<this.torres.get(destino).checkTop().getTama�o()) {
		Disco aux=this.torres.get(origen).sacarDisco();
		if(this.torres.get(destino).meterDisco(aux)) {
			result=true;
		}
		}else if (this.torres.get(destino).checkTop().getTama�o()==0) {
			Disco aux=this.torres.get(origen).sacarDisco();
			this.torres.get(destino).meterDisco(aux);
			}
		
		return result;
	}
	public void comprobarTerminado(){
		if(this.torres.get(2).getCapacidad()==this.nDiscos) {
			this.setTerminado(true);
		}
	}
}

