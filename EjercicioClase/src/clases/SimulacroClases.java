package clases;

import java.util.Random;

public class SimulacroClases {
	private double altura;
	private double peso;
	boolean boolSexo;
	private char sexo;
	private int edad;
	private String clOMS;
	private double IMC;
	private String DNI;
	
	public SimulacroClases() {
		
	}
	public SimulacroClases(double altura, double peso) {
		this.altura=altura;
		this.peso=peso;
		this.DNI=this.genDNI();
	}
	public SimulacroClases(double altura, double peso, boolean boolSexo, int edad) {
		this.altura=altura;
		this.peso=peso;
		this.boolSexo=boolSexo;
		this.edad=edad;
		this.sexo=this.calcularSexo();
		this.DNI=this.genDNI();
	}
	public char calcularSexo() {
		if(boolSexo) {
			sexo='M';
		}else {
			sexo='F';
		}
		return sexo;
	}
	public void compSexo(boolean comp) {
		if(this.boolSexo==comp) {
			System.out.println("El sexo introducido concuerda con el almacenado.");
		}else {
			System.out.println("El sexo introducido no concuerda con el almacenado.");
		}
	}
	public void compMayor(){
		if (edad>=18) {
			System.out.println("Es mayor de edad.");
		}else {
			System.out.println("No es mayor de edad.");
		}
	}
	public double calcIMC() {
		IMC=peso/(altura*altura);
		return IMC;
	}
	public String obtClOms() {
		this.calcIMC();
		if (IMC<18.5) {
			clOMS="Bajo peso";
		}else if (IMC>=18.5&&IMC<24.9) {
			clOMS="Adecuado";
		}else if (IMC>=24.9&&IMC<29.9) {
			clOMS="Sobrepeso";
		}else if (IMC>=29.9&&IMC<34.9) {
			clOMS="Obesidad grado 1";
		}else if (IMC>=34.9&&IMC<39.9) {
			clOMS="Obesidad grado 2";
		}else if (IMC>=39.9) {
			clOMS="Obesidad grado 3";
		}else {
			clOMS="IMC por debajo de los parámetros reconocidos";
		}
		return clOMS;
	}
	public String genDNI() {
		Random rnd = new Random();
		int numDNI= rnd.nextInt(99999999);
	  	final String LISTA = "TRWAGMYFPDXBNJZSQVHLCKE";
        int DNIletra = numDNI%23; //Hacemos el modulo de la letra para cogerla
        char letra = LISTA.charAt(DNIletra);
        DNI = numDNI+""+letra;
		return DNI;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public boolean isBoolSexo() {
		return boolSexo;
	}
	public void setBoolSexo(boolean boolSexo) {
		this.boolSexo = boolSexo;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getClOMS() {
		return clOMS;
	}
	public double getIMC() {
		return IMC;
	}
	public void setIMC(double iMC) {
		IMC = iMC;
	}
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}

}
