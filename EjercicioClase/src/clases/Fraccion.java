package clases;

public class Fraccion {
	private int numerador;
	private int denominador;

	public Fraccion() {
		
	}
	
	public Fraccion(int numerador, int denominador) {
		this.numerador=numerador;
		this.denominador=denominador;
	}
	
	public Fraccion sumar(Fraccion fraccion2) {
		int num=(this.numerador*fraccion2.denominador)+(fraccion2.numerador*this.denominador);
		int denom=(this.denominador*fraccion2.denominador);
		Fraccion fraccionResult=new Fraccion(num, denom);
		return fraccionResult;
	}
	public Fraccion restar(Fraccion fraccion2) {
		int num=(this.numerador*fraccion2.denominador)-(fraccion2.numerador*this.denominador);
		int denom=(this.denominador*fraccion2.denominador);
		Fraccion fraccionResult=new Fraccion(num, denom);
		return fraccionResult;
	}
	public Fraccion multiplicar(Fraccion fraccion2) {
		int num=(this.numerador)*(fraccion2.numerador);
		int denom=(this.denominador*fraccion2.denominador);
		Fraccion fraccionResult=new Fraccion(num, denom);
		return fraccionResult;
	}
	public Fraccion dividir(Fraccion fraccion2) {
		int num=(this.numerador*fraccion2.denominador);
		int denom=(this.denominador*fraccion2.numerador);
		Fraccion fraccionResult=new Fraccion(num, denom);
		return fraccionResult;
	}
	public void uwu() {
		int dado=(int) (Math.random()*20)+1;
		System.out.println("El cangrejo Sebast�an te intenta hacer presa. \nTirada: "+dado+".");
		if (dado==20) {
			System.out.println("�Cr�tico! Te jodiste bro, prepar�te para la palisa");
		}else {
			System.out.println("Fall�, �qu� co�o esperabas de un cangrejo CR1/8, bro?");
		}
	}
	
	public int getNumerador() {
		return numerador;
	}

	public void setNumerador(int numerador) {
		this.numerador = numerador;
	}

	public int getDenominador() {
		return denominador;
	}

	public void setDenominador(int denominador) {
		this.denominador = denominador;
	}
	
	@Override
	public String toString() {
		return this.numerador+"/"+this.denominador;
	}
}

