package clases;

import java.util.ArrayList;
import java.util.Collections;

public class Mazo {
	private ArrayList<Carta> cartas;
	private int nCartas;
	
	public Mazo() {
		this.nCartas=54;
		this.cartas = new ArrayList<Carta>();
	for(int i=0;i<4;i++) {
		for(int j=0;j<13;j++) {
			if(j<10) {
			this.cartas.add(new Carta(i,(j+1)));
			}else if(j==10){
				this.cartas.add(new Carta(i,"Jota"));
			}else if(j==11){
				this.cartas.add(new Carta(i,"Reina"));
			}else if(j==12){
				this.cartas.add(new Carta(i,"Rey"));
			}
		}
	}
}
	
	public void barajar() {
		Collections.shuffle(cartas);
	}
	public Carta sacarCarta() {
		return this.cartas.get(0);
	}
	public void eliminarCarta() {
		this.cartas.remove(0);
		this.nCartas--;
	}
	
	public ArrayList<Carta> getCartas() {
		return cartas;
	}

	public void setCartas(ArrayList<Carta> cartas) {
		this.cartas = cartas;
	}

	public int getnCartas() {
		return nCartas;
	}

	public void setnCartas(int nCartas) {
		this.nCartas = nCartas;
	}

	@Override
	public String toString() {
	return "Tu mazo tiene "+nCartas+" cartas y son "+cartas.toString();	
	}
}

