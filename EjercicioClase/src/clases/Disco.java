package clases;

public class Disco {
	private int tama�o;
	
	//Constructor
	public Disco() {
		this.tama�o=0;
		
	}
	public Disco(int tama�o) {
		this.tama�o=tama�o;
	}
	
	//Getters y Setters
	public int getTama�o() {
		return this.tama�o;
	}
	public void setTama�o(int tama�o) {
		this.tama�o = tama�o;
	}
	
	//M�todos
	@Override
	public String toString() {
		String res="";
		for (int i=0;i<this.tama�o;i++) {
			res+='*';
		}
		return res;
	}

}
