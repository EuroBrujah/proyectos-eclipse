package clases;

public class DNI {
	private String nombre;
	private String apellido1;
	private String apellido2;
	private int numero;
	private char letra;
	
	public DNI() {
		
	}
	public DNI(int numero) {
		this.numero=numero;
		this.letra=this.genLetra();
	}
	public DNI(String nombre, String apellido1, String apellido2) {
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
	}
	public DNI (String nombre, String apellido1, String apellido2, int numero) {
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.numero=numero;
		this.letra=this.genLetra();
	}
	
	public char genLetra() {
		  	final String LISTA = "TRWAGMYFPDXBNJZSQVHLCKE";
	        int DNIletra = this.numero%23; //Hacemos el modulo de la letra para cogerla
	        letra = LISTA.charAt(DNIletra); // Buscamos la letra en la posicion indicada
	        return letra;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
		this.letra = this.genLetra();
	}
	public char getLetra() {
		return letra;
	}
	public void setLetra(char letra) {
	  	final String LISTA = "TRWAGMYFPDXBNJZSQVHLCKE";
        int DNIletra = this.numero%23; //Hacemos el modulo de la letra para cogerla
        char letraC = LISTA.charAt(DNIletra); // Buscamos la letra en la posicion indicada
        if(letraC==letra) {
		this.letra = letra;
        }else {
        	System.out.println("La letra no corresponde al n�mero introducido.");
        }
	}
}
