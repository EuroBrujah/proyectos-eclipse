package clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class EstudianteProgramacion {

	private String nombre;
	private String apellido1;
	private String apellido2;
	private int gradoCalvicie;
	private String gradoCalvicieImp;
	
	public EstudianteProgramacion() {
		this.generarNombre();
		this.generarCalvicie();
	}
	public EstudianteProgramacion(String nombre) {
		this.nombre=nombre;
		this.generarCalvicie();
		
	}
	public EstudianteProgramacion(String nombre, String apellido1, String apellido2) {
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.generarCalvicie();
	}
	public void ejercicioRealizar() {
		
	}
	public void quejarse() {
		ArrayList<String> quejas = new ArrayList<String>();
		quejas.add("Me cago en San Dios.");
		quejas.add("�Que hace este pelo en mi mesa?");
		quejas.add("*Mira al frente como si tuviese flasbacks de Vietnam.*");
		quejas.add("Los de marketing otra vez, con su puta madre.");
		quejas.add("No soporto esa risa.");
		quejas.add("Illo, callarse los de atr�s.");
		quejas.add("*Golpe a la mesa.*");
		quejas.add("Puta madre, el punto y coma.");
		quejas.add("Esto en C era mejor.");
		quejas.add("Enrique, yo no me entero de na.");
		Collections.shuffle(quejas);
		System.out.println("El alumno "+this.nombre+" dice: "+quejas.get(0));
	}
	public void hacerPreguntaEstupida() {
		ArrayList<String> preguntas = new ArrayList<String>();
		preguntas.add("�Profesor, que significa esto, que soy retrasado y no s� leer errores de Java?");
		preguntas.add("Profesor, �Como se hace esto que aprendimos en el tema 3 cuando no estaba prestando puta mierda de atenci�n?");
		preguntas.add("Profesor, �Que tenemos que hacer? Que aunque acabes de explicarlo estaba con el LoL/hablando/pel�ndomela fuertemente.");
		preguntas.add("Pero Enrique, �el constructor vac�o para que sirve?");
		preguntas.add("Enrique, expl�came esto que podr�a encontrar buscando aproximadamente 5 segundos en Google, por favor.");
		Collections.shuffle(preguntas);
		System.out.println("El alumno "+this.nombre+" dice: "+preguntas.get(0));

		
	}
	public void meterRuido() {
		String letras = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ";
		String ruidoRand = "";
		Random rnd = new Random();
		for(int i=0;i<10;i++) {
			int letraRand = rnd.nextInt(27);
			ruidoRand += letras.charAt(letraRand);
		}
		System.out.println("La clase profiere un: "+ruidoRand);
		
	}
	public void descanso() {
		ArrayList<String> descanso = new ArrayList<String>();
		descanso.add("�Profesor, puedo ir a cagar? *Se pone el abrigo*.");
		descanso.add("Bueno, va siendo hora de ir a desayunar, �no?");
		descanso.add("Profesor, �nos podemos ir ya?");
		descanso.add("�Un descansito, no?");
		Collections.shuffle(descanso);
		System.out.println("El alumno "+this.nombre+" dice: "+descanso.get(0));

	}
	public void generarCalvicie() {
		Random rnd = new Random();
		gradoCalvicie=rnd.nextInt(3);
		if(gradoCalvicie==0) {
			gradoCalvicieImp="Pelo frondoso";
		}else if(gradoCalvicie==1) {
			gradoCalvicieImp="Se empieza a ver cart�n";
		}else if(gradoCalvicie==2) {
			gradoCalvicieImp="Bola de billar";
			
		}
	}
	public void generarNombre() {
		ArrayList<String> nombres = new ArrayList<String>();
		nombres.add("Eva");
		nombres.add("Rafael");
		nombres.add("�lvaro");
		nombres.add("Jes�s");
		nombres.add("Alonso");
		nombres.add("Jose Miguel");
		nombres.add("Alejandro");
		nombres.add("Fernando");
		nombres.add("Javier");
		nombres.add("Alfonso");
		nombres.add("Ra�l");
		nombres.add("Daniel");
		nombres.add("Eduardo");
		nombres.add("Juan Carlos");
		nombres.add("Claudia");
		nombres.add("Domingo");
		nombres.add("Jennifer");
		nombres.add("Iv�n");
		nombres.add("Gonzalo");
		nombres.add("Jose Antonio");
		nombres.add("Sergio");
		nombres.add("Roberto");
		nombres.add("Paula");
		Collections.shuffle(nombres);
		this.nombre = nombres.get(0);
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public int getGradoCalvicie() {
		return gradoCalvicie;
	}
	public void setGradoCalvicie(int gradoCalvicie) {
		this.gradoCalvicie = gradoCalvicie;
	}
	public String getGradoCalvicieImp() {
		return gradoCalvicieImp;
	}
	public void setGradoCalvicieImp(String gradoCalvicieImp) {
		this.gradoCalvicieImp = gradoCalvicieImp;
	}
}
