package clases;

import java.util.Scanner;

public class MainTriangulo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String resp = "";
		double respuesta = 0;
		double longB=0;
		double longA=0;
		double longC=0;
		String tipo = "";
		
		
		do {
			System.out.println("Bienvenido al calculador de tri�ngulos.\nIntroduzca la base del tri�ngulo:");
			respuesta=sc.nextDouble();
			longB=respuesta;
			System.out.println("A continuaci�n introduzca el largo de uno de sus lados.");
			respuesta=sc.nextDouble();
			longA=respuesta;
			System.out.println("Finalmente, introduzca el largo de otro de sus lados");
			respuesta=sc.nextDouble();
			longC=respuesta;
			
			Triangulo tuTriangulo = new Triangulo (longA, longB, longC);
			if(tuTriangulo.isEquilatero()) {
				tipo="equil�tero";
			}else if(tuTriangulo.isIsosceles()) {
				tipo="is�sceles";
			}else if(tuTriangulo.isEscaleno())
				tipo="escaleno";
			System.out.println("Has introducido un tri�ngulo "+tipo+".\nSu per�metro es "+tuTriangulo.getPerimetro()+".\nSu �rea es "+tuTriangulo.getArea()+".");
			System.out.println("�Quiere calcular otro tri�ngulo?");
			resp=sc.next();
		}while (resp.equalsIgnoreCase("Si")||resp.equalsIgnoreCase("S"));
sc.close();
	}

}
