package clases;

import java.util.Scanner;

public class MainFecha {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int dia=0;
		int mes=0;
		int a�o=0;
		boolean terminado=true;
		
		do{
			terminado=true;
		System.out.println("Introduzca el d�a del mes: (1-31)");
		dia=sc.nextInt();
		System.out.println("Introduzca el mes: (1-12)");
		mes=sc.nextInt();
		System.out.println("Introduzca el a�o: ");
		a�o=sc.nextInt();
		
		if(dia<1 || dia>30) {
			System.out.println("Introduzca un d�a entre 1 y 30.");
			terminado=false;
		}else {

		}
		
		if(mes<0 || mes>12) {
			System.out.println("Introduzca un mes entre 1 y 12.");
			terminado=false;
		}else {

		}
		
		if(mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12) {
			
		}else if (mes==2){
			if(dia>28) {
				terminado=false;
			}
		}else {
			if (dia>30) {
				terminado=false;
			}
		}
		}while(!terminado);
		Fecha fecha1 = new Fecha (dia, mes, a�o);
		Fecha fecha2 = new Fecha (31, 12, 1994);
		System.out.println("Fecha 1: "+fecha1.toString());
		System.out.println("Fecha 2: "+fecha2.toString());
		
		fecha1.decrementar();
		System.out.println("Decrementamos fecha 1: "+fecha1.toString());
		fecha2.incrementar();
		System.out.println("Incrementamos fecha 2: "+fecha2.toString());
		
sc.close();
	}

}
