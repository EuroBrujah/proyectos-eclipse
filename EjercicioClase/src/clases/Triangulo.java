package clases;

public class Triangulo {
	//metodo perimetro, metodo area, tres metodos mas para cada tipo de triangulo
	private double longA;
	private double longB;
	private double longC;
	private double perimetro;
	private double area;
	private double base;
	private double altura;
	private boolean isEquilatero=false;
	private boolean isIsosceles=false;
	private boolean isEscaleno=false;
	
	public boolean isEquilatero() {
		if(longA==longB && longB==longC) {
			this.isEquilatero = true;
		}
		return this.isEquilatero;
	}
	public boolean isIsosceles() {
		if((longA==longB && longB!=longC) || (longA!=longB && longB==longC) || (longA==longC && longA!=longB)) {
			this.isIsosceles = true;
		}
		return this.isIsosceles;
	}
	public boolean isEscaleno() {
		if(longA!=longB && longB!=longC && longC!=longA) {
			this.isEscaleno = true;
		}
		return this.isEscaleno;
	}
	
	public double calcularAltura() {
		if (this.isEquilatero) {
			this.altura= (double) Math.round((double)(Math.sqrt(3)*longA)/2*100)/100;
		}
		if (this.isIsosceles) {
			this.altura= (double) Math.round((double)(Math.sqrt((Math.pow(longA, 2))-((Math.pow(longB, 2))/4)))*100)/100;
		}
		if (this.isEscaleno) {
			double S = (longA+longB+longC)/2;
			this.altura= (double) Math.round((double)(2/longB)*Math.sqrt(S*(S-longA)*(S-longB)*(S-longC))*100)/100;
		}
		return this.altura;
	}
	public double perimetro() {
		this.perimetro = (double) Math.round((double)(longA+longB+longC)*100)/100;
		return this.perimetro;
	}
	public double area() {
		this.area = (base * altura)/2;
				return this.area;
	}
	public Triangulo (double longA, double longB, double longC) {
		this.longA=longA;
		this.longB=longB;
		this.longC=longC;
		this.base=longB;
		this.isEquilatero=isEquilatero();
		this.isIsosceles=isIsosceles();
		this.isEscaleno=isEscaleno();
		this.altura = calcularAltura();
		this.perimetro=perimetro();
		this.area=area();
	}
	public double getLongA() {
		return longA;
	}
	public void setLongA(double longA) {
		this.longA = longA;
	}
	public double getLongB() {
		return longB;
	}
	public void setLongB(double longB) {
		this.longB = longB;
	}
	public double getLongC() {
		return longC;
	}
	public void setLongC(double longC) {
		this.longC = longC;
	}
	public double getPerimetro() {
		return perimetro;
	}
	public void setPerimetro(double perimetro) {
		this.perimetro = perimetro;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public double getBase() {
		return base;
	}
	public void setBase(double base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public void setEquilatero(boolean isEquilatero) {
		this.isEquilatero = isEquilatero;
	}
	public void setIsosceles(boolean isIsosceles) {
		this.isIsosceles = isIsosceles;
	}
	public void setEscaleno(boolean isEscaleno) {
		this.isEscaleno = isEscaleno;
	}
}
