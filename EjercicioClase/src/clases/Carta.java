package clases;

public class Carta {
	private int palo;
	private String paloImp;
	private int numero=0;
	private int valor;
	private String figura="";
	
	public Carta() {
		
	}
	public Carta(int palo, int numero) {
		this.palo=palo;
		this.numero=numero;
		this.conseguirValor();
		this.convertirPalo();
	}
	public Carta(int palo, String figura){
		this.palo=palo;
		this.figura=figura;
		this.conseguirValor();
		this.convertirPalo();
	}
	
	public void convertirPalo() {
		if (this.palo==0) {
			this.paloImp="Tr�boles";
		}else if (this.palo==1) {
			this.paloImp="Picas";
		}else if (this.palo==2) {
			this.paloImp="Corazones";
		}else if (this.palo==3) {
			this.paloImp="Diamantes";
		}
	}
	public void conseguirValor() {
		if(this.numero!=0) {
		this.valor=this.numero;
		}else {
			this.valor=10;
		}
	}
	public int getPalo() {
		return palo;
	}
	public void setPalo(int palo) {
		this.palo = palo;
	}
	public String getPaloImp() {
		return paloImp;
	}
	public void setPaloImp(String paloImp) {
		this.paloImp = paloImp;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getValor() {
		return valor;
	}
	public void setValor(int valor) {
		this.valor = valor;
	}
	public String getFigura() {
		return figura;
	}
	public void setFigura(String figura) {
		this.figura = figura;
	}
	@Override
	public String toString() {
		if(!figura.equals("Jota")&&!figura.equals("Reina")&&!figura.equals("Rey")) {
	return ""+this.numero+" de "+this.paloImp;	
	}else {
		return ""+this.figura+" de "+this.paloImp;
	}
	}
}
