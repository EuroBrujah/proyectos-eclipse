package clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ProfesorProgramacion {

	/*
	 * Profesor de Programaci�n, con los siguientes atributos y m�todos: Nombre
	 * Apellidos Edad Grado de belleza suprema explicar() - Genera una explicaci�n
	 * aleatoria (de una pool que hag�is) echarSprayDesinfectante() - De tipo
	 * booleano, aleatoriamente se decidir� si se le va la pinza o no.
	 * mandarUnEjercicio()- Genera un ejercicio aleatorio para sus alumnos (de una
	 * pool que hag�is). llorarDesesperado() - Reacci�n est�ndar cuando recibe una
	 * pregunta.
	 */
	private String nombre;
	private String apellido1;
	private String apellido2;
	private int edad;
	private int gradoBellezaSuprema;
	private String gradoBellezaSupremaImp;
	
	public ProfesorProgramacion() {
		
	}
	public void explicar() {
		ArrayList<String> explicaciones = new ArrayList<String>();
		explicaciones.add("Esa duda viene en el PDF");
		explicaciones.add("No te puedo explicar con el Minecraft/Terraria/Lolito abierto");
		explicaciones.add("Si, eso se hace : *Introduzca palabras en profundo*");
		Collections.shuffle(explicaciones);
		System.out.println("El profesor "+this.nombre+" dice "+explicaciones.get(0));
	}
	public void echarSprayDesinfectante() {
		Random rnd = new Random();
		boolean echar = rnd.nextBoolean();
		if(echar) {
			System.out.println("Al profesor "+this.nombre+" se le va la pinza y echa spray desinfectante a Fernando");
		}else {
			System.out.println("Al profesor "+this.nombre+" no se le va la pinza... Aun...");
		}
	}
	public void mandarUnEjercicio() {
		ArrayList<String> ejercicios = new ArrayList<String>();
		ejercicios.add("Cread la clase 'Movida Matem�tica para que Javi llore' y...");
		ejercicios.add("Esta es sencillita, vamos a crear...");
		Collections.shuffle(ejercicios);
		System.out.println("El profesor "+this.nombre+" manda el siguiente ejercicio: "+ejercicios.get(0));
	}
	public void llorarDesesperado() {
		Random rnd = new Random();
		int n = rnd.nextInt(9999);
		System.out.println("El profesor llora desesperado ante la "+n+"� vez que le hacen la misma pregunta.");
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getGradoBellezaSuprema() {
		return gradoBellezaSuprema;
	}
	public void setGradoBellezaSuprema(int gradoBellezaSuprema) {
		this.gradoBellezaSuprema = gradoBellezaSuprema;
	}
	public String getGradoBellezaSupremaImp() {
		return gradoBellezaSupremaImp;
	}
	public void setGradoBellezaSupremaImp(String gradoBellezaSupremaImp) {
		this.gradoBellezaSupremaImp = gradoBellezaSupremaImp;
	}

}
