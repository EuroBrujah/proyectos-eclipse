package clases;

public class MainLinea {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Linea linea1 = new Linea();
		System.out.println("L�nea1: "+linea1);
		linea1.desD(20);
		System.out.println("L�nea1 diagonal ascendente: "+linea1);
		
		Linea linea2 = new Linea(15,75,12,37);
		System.out.println("L�nea2: "+linea2);
		linea2.desH(12);
		System.out.println("L�nea2 desplaza a la derecha: "+linea2);
		
		Linea linea3 = new Linea(15,75);
		System.out.println("L�nea3: "+linea3);
		linea3.desV(15);
		System.out.println("L�nea3 desplaza arriba: "+linea3);
	}

}
