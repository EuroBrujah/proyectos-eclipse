package clases;

import java.util.ArrayList;

public class PilaOrdenada {
	private ArrayList<Disco> discos;
	private int capacidad;
	
	//Constructores
	public PilaOrdenada() {
		this.discos=new ArrayList<Disco>();
		this.capacidad=0;
	}

	//Getters y Setters
	public ArrayList<Disco> getDiscos() {
		return discos;
	}

	public int getCapacidad() {
		return capacidad;
	}
	
	//M�todos
	
	@Override
	public String toString() {
		String res="";
		for(int i=0;i<this.capacidad;i++) {
			res +=this.discos.get(i) + "\n";
		}
		return res;
	}
	
	public boolean meterDisco(Disco disco) {
		boolean res=false;
		if(this.capacidad!=0) {
			if(this.discos.get(0).getTama�o()>disco.getTama�o()){
				this.discos.add(0, disco);
				this.capacidad++;
				res=true;
				}
		}else {
			res=true;
			this.discos.add(0,disco);
			this.capacidad++;
		}
		return res;
	}
	
	public Disco sacarDisco() {
		Disco aux = this.discos.get(0);
		this.discos.remove(0);
		this.capacidad--;
		return aux;	
	}
	
	public Disco checkTop() {
		Disco aux = new Disco(0);
		Disco res;
		if(this.capacidad==0) {
			res=aux;
		}else {
		res = this.discos.get(0);
		}
		return res;
	}
}