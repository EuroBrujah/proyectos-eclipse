package clases;

import java.util.Scanner;

public class MainTorreHanoi {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		boolean res = false;
		
		JuegoHanoi partida = new JuegoHanoi(4);
		
	do {
		System.out.println("Estado de la partida:\n"+partida+".");
		System.out.println("�Qu� desea hacer?\n1. Mover.\n2. Comprobar la pieza superior de una torre.");
		int opcion = sc.nextInt();
		switch (opcion){
		case 1:
			do {
			res =false;
			System.out.println("�Desde que torre desea mover?");
			int origen=sc.nextInt();
			System.out.println("�En que pila desea colocarlo?");
			int destino=sc.nextInt();
			if(origen>0&&origen<4&&destino>0&&destino<4) {
			partida.mover(origen-1, destino-1);
			res = true;
			}else {
				System.out.println("Selecciones torres v�lidas.");
			}
			}while(!res);
			break;
		case 2:
			System.out.println("�Qu� torre desea comprobar?");
			int consulta = sc.nextInt();
			System.out.println(partida.getTorres().get(consulta-1).checkTop());
			break;
		default:
			System.out.println("Por favor, introduzca una de las opciones indicadas.");
			break;
		}
		partida.comprobarTerminado();
	}while(!partida.isTerminado());
	System.out.println("Enhorabuena, has ganado");
	sc.close();
}
}
