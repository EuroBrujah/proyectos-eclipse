package clases;

public class Dado {
	private int nCaras;
	private int valor;
	
	public Dado (int nCaras) {
		this.setnCaras(nCaras); 
		
	}
	public double tirar() {
		return valor = (int) (1+(Math.random()*nCaras));
	}

	public int getnCaras() {
		return nCaras;
	}

	public void setnCaras(int nCaras) {
		this.nCaras = nCaras;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}
