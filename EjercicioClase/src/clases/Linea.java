package clases;

public class Linea {
	private Punto puntoA = new Punto();
	private Punto puntoB = new Punto();
	
	public Linea() {
		
	}
	public Linea(Punto puntoA, Punto puntoB) {
		this.puntoA = puntoA;
		this.puntoB = puntoB;
		
	}
	public Linea(double x1, double y1, double x2, double y2) {
		puntoA = new Punto (x1,y1);
		puntoB = new Punto (x2,y2);
		
	}
	public Linea(double x, double y) {
		puntoA = new Punto (x,y);
		puntoB = new Punto (x,y);
	}
	public void desH (double mod) {
		this.puntoA.desX(mod);
		this.puntoB.desX(mod);
		
	}
	public void desV (double mod) {
		this.puntoA.desY(mod);
		this.puntoB.desY(mod);
	}
	public void desD (double mod) {
		this.puntoA.desXY(mod);
		this.puntoB.desXY(mod);
	}
	
	public Punto getPuntoA() {
		return puntoA;
	}
	public void setPuntoA(Punto puntoA) {
		this.puntoA = puntoA;
	}
	public Punto getPuntoB() {
		return puntoB;
	}
	public void setPuntoB(Punto puntoB) {
		this.puntoB = puntoB;
	}
	
	@Override
	public String toString() {
		return "["+puntoA+","+puntoB+"]";
	
}

}
