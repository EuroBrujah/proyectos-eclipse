package clases;

import java.util.Scanner;

public class MainPunto {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double mod=0;
		
		Punto punto1=new Punto (50,75);
		System.out.println(punto1);
		
		System.out.println("Introduzca cuanto quiere desplazar el punto horizontalmente: ");
		mod=sc.nextDouble();
		punto1.desX(mod);
		System.out.println(punto1);
		System.out.println("Introduzca cuanto quiere desplazar el punto verticalmente: ");
		mod=sc.nextDouble();
		punto1.desY(mod);
		System.out.println(punto1);
		System.out.println("Introduzca cuanto quiere desplazar el punto diagonalmente: ");
		mod=sc.nextDouble();
		punto1.desXY(mod);
		System.out.println(punto1);
		
		sc.close();
	}

}
