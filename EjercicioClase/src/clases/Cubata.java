package clases;

public class Cubata {
	//Crear m�todos llenar beber e hidalgo
	private int cantidad;
	private String tipoAlcohol;
	private String tipoRefresco;
	private String tipoVaso;
	private boolean hielo;
	
	
	public Cubata (int cantidad, boolean hielo, String tipoAlcohol, String tipoRefresco) {
		this.cantidad = cantidad;
		this.hielo = true;
		this.tipoAlcohol = tipoAlcohol;
		this.tipoRefresco = tipoRefresco;
	}
	
	public void llenar(){
		this.cantidad = 100;
	}
	public void beber() {
		this.cantidad -= 25;
		
	}
	public void hildago() {
		this.cantidad = 0;
	}

	public int getcantidad() {
		return cantidad;
	}

	public void setcantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getTipoAlcohol() {
		return tipoAlcohol;
	}

	public void setTipoAlcohol(String tipoAlcohol) {
		this.tipoAlcohol = tipoAlcohol;
	}

	public String getTipoVaso() {
		return tipoVaso;
	}

	public void setTipoVaso(String tipoVaso) {
		this.tipoVaso = tipoVaso;
	}

	public String getTipoRefresco() {
		return tipoRefresco;
	}

	public void setTipoRefresco(String tipoRefresco) {
		this.tipoRefresco = tipoRefresco;
	}

	public boolean isHielo() {
		return hielo;
	}

	public void setHielo(boolean hielo) {
		this.hielo = hielo;
	}
	
	@Override
	public String toString() {
	return "Tu cubata es de "+this.tipoAlcohol+" con "+this.tipoRefresco+". Est� al "+this.cantidad+"% de capacidad.";	
	}
}
