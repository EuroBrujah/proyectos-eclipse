package clases;

public class MainLibro {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Libro libro1 = new Libro ("9788420636269","Alicia en el Pa�s de las Maravillas","Lewis Carroll",184,"Cuentos",1865,"Alianza Editorial","Novela",false);
		Libro libro2 = new Libro ("9788490707838","Entrevista con el Vampiro","Anne Rice",384,"Fantas�a",1976,"B de Bolsillo","Novela",true);
		System.out.println(libro1.toString());
		System.out.println(libro2.toString());
		libro1.prestamo();
		libro2.prestamo();
		System.out.println(libro1.toString());
		System.out.println(libro2.toString());
		libro1.compararPag(libro2);
		libro1.devolver();
		libro2.devolver();
		System.out.println(libro1.toString());
		System.out.println(libro2.toString());
		libro2.devolver();
	}

}
