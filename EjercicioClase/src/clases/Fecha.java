package clases;

public class Fecha {
	//atributos: dia mes a�o
	//constructores, getter setter
	//sobrecarga toString
	//dd/mm/yyyy
	//metodo dia+1, metodo dia-1
	
	private int dia;
	private int mes;
	private int a�o;
	
	public Fecha() {
		
	}
	
	public Fecha(int dia, int mes, int a�o) {
		this.dia=dia;
		this.mes=mes;
		this.a�o=a�o;
	}
	
	public void incrementar() {
		if(mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12) {
		if(this.dia==31) {
			if(this.mes!=12) {
				this.mes++;
				this.dia=1;
			}else {
				this.a�o++;
				this.mes=1;
				this.dia=1;
			}
		}else {
		this.dia++;
		}
	}else if (mes==2) {
		if(this.dia==28) {
			if(this.mes!=12) {
				this.mes++;
				this.dia=1;	
			}else {
				this.a�o++;
				this.mes=1;
				this.dia=1;
			}
		}else {
			this.dia--;
		}
		
	}
		else{
			if(this.dia==30) {
				if(this.mes!=12) {
					this.mes++;
					this.dia=1;
				}else {
					this.a�o++;
					this.mes=1;
					this.dia=1;
				}
			}else {
			this.dia++;
			}
		}
	}
	public void decrementar() {
		if(mes==1||mes==3||mes==5||mes==7||mes==8||mes==10||mes==12) {
		if(this.dia==1) {
			if(this.mes!=1) {
				this.mes--;
				this.dia=31;	
			}else {
				this.a�o--;
				this.mes=12;
				this.dia=31;
			}
		}else {
			this.dia--;
		}
		
	}else if (mes==2) {
		if(this.dia==1) {
			if(this.mes!=1) {
				this.mes--;
				this.dia=28;	
			}else {
				this.a�o--;
				this.mes=12;
				this.dia=28;
			}
		}else {
			this.dia--;
		}
		
	}
		else {
		if(this.dia==1) {
			if(this.mes!=1) {
				this.mes--;
				this.dia=31;	
			}else {
				this.a�o--;
				this.mes=12;
				this.dia=31;
			}
		}else {
			this.dia--;
		}
	}
		}
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getA�o() {
		return a�o;
	}
	public void setA�o(int a�o) {
		this.a�o = a�o;
	}
	
	@Override
	public String toString() {
		return "Es el d�a "+this.dia+" del mes "+this.mes+" del a�o "+this.a�o+".";
	}

}
