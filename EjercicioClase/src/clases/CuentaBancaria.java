package clases;

public class CuentaBancaria {
	
	private String titular;
	private double saldo;
	private double operacion;
	
	public CuentaBancaria() {
		
	}
	public CuentaBancaria(String titular) {
		this.titular=titular;
		this.saldo=0;
	}
	public double getOperacion() {
		return operacion;
	}
	public void setOperacion(double operacion) {
		this.operacion = operacion;
	}
	public CuentaBancaria(String titular, double saldo) {
		this.titular=titular;
		this.saldo=saldo;
	}
	
	public void ingresar(double operacion) {
		if(operacion>0) {
			if(operacion<600) {
		this.saldo += operacion;
			}else {
				System.out.println("L�mite de retirada excedido. Introduzca una cantidad menor de 600�.");
			}
		}else {
			System.out.println("La cantidad introducida es negativa. Por favor, introduzca una cantidad positiva.");
		}
	}
	public void retirar(double operacion) {
		if(saldo>operacion) {
		this.saldo -= operacion;
		}else {
			System.out.println("La cantidad de saldo disponible es inferior a la que intenta retirar.");
		}
	}
	public void transferencia (CuentaBancaria cuenta2, double operacion) {
		this.saldo -= operacion;
		cuenta2.saldo += operacion;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	@Override
	public String toString() {
		return "Titular de la cuenta: "+titular+".\nSaldo: "+saldo+"�";
	}
}
