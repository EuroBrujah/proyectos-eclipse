package excepciones;

import java.util.Scanner;

public class TryCatch {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		int array[] = new int[4];
		boolean terminar = false;
		int contador = 0;

		System.out.println("Introduzca algo: ");
		do {
			try {
				array[5] = sc.nextInt();
			} catch (ArrayIndexOutOfBoundsException error) {
				System.out.println(error);
				System.out.println("Introduzca una posici�n de memoria permitida (m�x 4)");
				contador++;
				if (contador == 5) {
					terminar = true;
				}
			}
		} while (!terminar);
		sc.close();
	}
}
