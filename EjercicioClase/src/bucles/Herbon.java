package bucles;

import java.util.Scanner;

public class Herbon {

	public static void main(String[] args) {
		// Lee un string y devuelve la cadena a la inversa. Tambi�n te dice cuantas vocales hay.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		System.out.println("Introduzca una frase para darle la vuelta: ");
		String frase = sc.next();
		//Solicitamos al usuario una frase y la almacenamos en una string.
		String frase2 = "";
		int vocales = 0;
		//Inicializamos las variables para almacenar la string a la inversa y el contador de vocales.

			for(int i=frase.length()-1;i>-1;i--) {
				char abc = frase.charAt(i);
				frase2 += abc;
				//Creamos un bucle for inverso, desde el largo de la cadena hasta su �ltima posici�n, extraemos cada char
				//y lo vamos almacenando en la variable string que hemos creado
				
			if (abc == 'a' || abc =='e' || abc =='i' || abc == 'o' || abc =='u' || abc =='�' || abc =='�' || abc =='�' || abc =='�' || abc =='�' || abc =='A' || abc =='E' || abc =='I' || abc =='O' || abc =='U') {
					vocales++;
					//Comprobamos si el char es vocal y aumentamos el contador en caso de que lo sea.
				}
			}
			System.out.println(""+frase+" al rev�s es "+frase2+".\nLa frase tiene "+vocales+" vocales.");
			//Imprimimos nuestra frase a la inversa y el contador de vocales.
			sc.close();
			//Cerramos teclado.
		
	}

}
