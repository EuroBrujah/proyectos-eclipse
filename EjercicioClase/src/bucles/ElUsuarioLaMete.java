package bucles;

import java.util.Scanner;

public class ElUsuarioLaMete {

	public static void main(String[] args) {
		// El usuario mete un n�mero por teclado y tienes que decir si n es primo o no.
		// El usuario mete un n�mero y tienes que dar todos los primos desde 1 hasta n.
		Scanner sc = new Scanner(System.in);
		int num;


		System.out.println("Introduzca un n�mero para saber si es primo: ");
		num = sc.nextInt();
		System.out.println("Los n�meros primos desde "+num+" hasta 1 son: ");
		for (int j = 1; j <=num; j++) {
			boolean primo = true;

		for (int i = 2; i < j; i++) {

			if (j % i == 0) {
				primo = false;
			}
		}
		if (primo) {
			System.out.println(j);

		} 

		}
		sc.close();
		
		
	}		
}