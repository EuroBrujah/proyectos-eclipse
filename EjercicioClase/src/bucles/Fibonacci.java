package bucles;

import java.util.Scanner;

public class Fibonacci {

	public static void main(String[] args) {
		//Solicitas al usuario un n�mero y le tienes que pedir los n terminos de la serie de Fibonacci correspondientes.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado
		System.out.println("Introduzca un n�mero: ");
		int n = sc.nextInt();
		//Solicitamos al usuario un n�mero por teclado y lo almacenamos.
		int fib = 0;
		int fib2 = 1;
		int aux1 = 0;
		//Inicializamos los dos primeros t�rminos de la serie de fibonacci y un auxiliar.
		
		System.out.println("Los "+n+" primeros t�rminos de la serie de Fibonacci son: ");
		//Imprimimos el mensaje de resultado
		for (int i=0;i<n;i++) {
			//El for crea tantas iteraciones como el n�mero introducido por el usuario.
			if (i==0) {
				System.out.println(fib);
				//En la primera iteraci�n siempre imprime el primer t�rmino de Fibonacci.
			}else if (i==1){
				System.out.println(fib2);
				//En la segunda iteraci�n siempre imprime el segundo t�rmino de Fibonacci.
			}else{
				fib = (fib+fib2);
				System.out.println(fib);
				//En el resto de iteraciones suma los dos t�rminos anteriores e imprime el resultado.
				aux1 = fib;
				fib = fib2;
				fib2 = aux1;
				//Una vez imprimido el resultado, almacenamos el t�rmino mayor en un auxiliar, reemplazamos por el menor y volvemos 
				//a extraer el t�rmino del auxiliar.
			}
		}
		sc.close();
		//Cerramos teclado.
	}

}
