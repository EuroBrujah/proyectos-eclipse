package bucles;

import java.util.Scanner;

public class Asteriscos {

	public static void main(String[] args) {
		/* Dibujar la siguiente figura usando bucles:
		 *  ******
		 *  *
		 *  *
		 *  ******
		 *       *
		 *       *
		 *  ******
		 */
		System.out.println("Introduzca el n�mero de caracteres que quiere que tenga el dibujo: ");
		Scanner sc = new Scanner(System.in);
		String stFor = "";
		int j = sc.nextInt();
		//Creamos una variable para almacenar el dibujo.
		for (int it=0;it<=(j/16);it++) {
		for (int i=0;i<=15;i++) {
			stFor += "*";
			//El bucle For pone un asterisco en cada iteraci�n.
			if (i == 5 || i == 6 || i==7 || i==13 || i==14 || i==15) {
				stFor += "\n";
				//En determinadas l�neas, introduce un salto de l�nea tras el asterisco.
			} if (i == 13 || i==14 ) {
				stFor += "     ";
				//En determinadas l�neas introduce espacios tras el asterisco.
			}
		}
		}
		stFor += "******";
		System.out.println(stFor);
		//Una vez terminado, imprimimos el resultado.
		sc.close();
		//Cerramos teclado.
	}
}
