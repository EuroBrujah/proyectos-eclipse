package bucles;
import java.util.Scanner;

public class Inverso {

	public static void main(String[] args) {
		// Solicitamos al usuario un n�mero y lo imprimimos d�ndole la vuelta.
		Scanner sc = new Scanner (System.in);
		//Inicializamos teclado.
		System.out.println("Introduce un n�mero para darle la vuelta: ");
		int n = sc.nextInt();
		//Solicitamos al usuario un n�mero por teclado y lo almacenamos.
		int numOriginal = n;
		//Almacenamos el n�mero original para uso posterior
		int aux = 0;
		//Inicializamos aux.
		int cifras = 0;
		//Contador de cifras.
		aux = n;
		//Igualamos el aux al n�mero introducido.
		int rev = 0;
		//Inicializamos una variable para almacenar el n�mero inverso.
		
		do {
				aux= aux/10;
				cifras++;
		}while (aux!=0);
		//Este do while aumenta aumenta el contador de cifras por cada cifra del n�mero original.
		aux = 0;
		//Reiniciamos el contador.
		
		for (int i=cifras;i>0;i--) {
			//For con tantas iteraciones como el n�mero de cifras.
			aux = n%10;
			//Almacenamos en el aux la �ltima cifra del n�mero original.
			rev = rev * 10 + aux;
			//Almacenamos el n�mero en el n�mero inverso y multiplicamos por 10.
			n/=10;
			//Dividimos el n�mero original entre 10 para pasar a la siguiente cifra en la pr�xima iteraci�n.
		}
		System.out.println("Si invertimos el n�mero "+numOriginal+" el resultado es "+rev+".");
		if(numOriginal==rev) {
			System.out.println("El n�mero es capic�a.");
		}else {
			System.out.println("El n�mero no es capic�a.");
		}
		//Imprimimos n�mero original y el resultado de la inversi�n. As� como si es, o no, capic�a.
		sc.close();	
		//Cerramos teclado.
		}
	}


