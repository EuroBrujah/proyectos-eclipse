package bucles;

import java.util.Scanner;

public class Sumatoria {

	public static void main(String[] args) {
		// Pedir un valor por teclado y calcular su sumatoria.
		Scanner sc = new Scanner(System.in);
		//Inicializamos el teclado
		System.out.println("Introduzca un n�mero para conocer su sumatoria: ");
		int num = sc.nextInt();
		//Solicitamos al usuario un n�mero por teclado y lo almacenamos.
		int sumat = 0;
		int fact = 1;
		//Inicializamos una variable para almacenar la sumatoria y otra para su factorial.
		
		for (int i=num;i>=1;i--) {
			sumat = sumat +i;
			fact = fact *i;
			//El bucle corre desde el n�mero introducido por el usuario hasta 1, almacenando el valor de i en la variable
			//y despu�s reduci�ndolo en 1. En el caso del factorial, el valor de i se multiplica.
		}
		System.out.println("La sumatoria de "+num+" es "+sumat+".\nSu factorial es "+fact+".");
		sc.close();
		//Imprimimos el resultado de la sumatoria y del factorial y cerramos teclado.
	}

}
