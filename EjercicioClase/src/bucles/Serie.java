package bucles;

import java.util.Scanner;

public class Serie {

	public static void main(String[] args) {
		// Imprimir todos los n�meros desde 0 hasta el n�mero introducido por el
		// usuario.
		Scanner sc = new Scanner(System.in);
		// Inicializamos Scanner

		System.out.println("Introduzca un n�mero entero: ");
		int num = sc.nextInt();
		// Solicitamos al usuario un n�mero entero y lo almacenamos

		for (int i = 0; i <= num; i++) {
			System.out.println(i);
			// Corremos el bucle desde 0 hasta el n�mero introducido por el usuario,
			// imprimiento cada iteraci�n.
		}
		sc.close();
	}

}
