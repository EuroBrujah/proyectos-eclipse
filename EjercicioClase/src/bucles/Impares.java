package bucles;

import java.util.Scanner;

public class Impares {

	public static void main(String[] args) {
		// Introduce dos n�meros por teclado, imprime los impares que haya entre esos
		// dos n�meros.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		int n1 = 0;
		int n2 = 0;
		//Declaramos las variables en las que almacenaremos los n�meros a comprobar.
		
		do {
			System.out.println("Introduzca el primer n�mero: ");
			n1 = sc.nextInt();
			System.out.println("Introduzca el segundo n�mero: ");
			n2 = sc.nextInt();
			System.out.println("Ambos n�meros son iguales. Vuelva a introducir los n�meros.");
		} while (n1 == n2);
		//Este bucle solicita los n�meros al usuario hasta que meta dos n�meros distintos. As� evitamos cosas raras.

		System.out.println("Los n�meros impares entre " + n1 + " y " + n2 + " son:");
		//Nos preparamos para imprimir los resultados.
		if (n1 > n2) {
			for (int i = n2 + 1; i <= n1 - 1; i++) {
				if (i % 2 != 0) {
					System.out.println(i);
					//Bucle for en el caso en que el n1 sea el mayor. Recorre los n�meros entre los introducidos e imprime los impares
				}
			}
		} else if (n2 > n1) {
			for (int i = n1 + 1; i <= n2 - 1; i++) {
				if (i % 2 != 0) {
					System.out.println(i);
					//Bucle for en el caso en que el n1 sea el mayor. Recorre los n�meros entre los introducidos e imprime los impares
				}
			}
		}
		sc.close();
		//Cerramos teclado
	}
}
