package bucles;

import java.util.Scanner;

public class Pares {

	public static void main(String[] args) {
		// Pedimos al usuario dos n�meros enteros, imprimimos todos los n�meros pares
		// que haya entre el 2 y el 14
		Scanner sc = new Scanner(System.in);
		int i;
		//Inicializamos Scanner y variable i para el bucle

		System.out.println("Introduzca el primer n�mero entero: ");
		int num1 = sc.nextInt();
		System.out.println("Introduzca el segundo n�mero entero: ");
		int num2 = sc.nextInt();
		//Solicitamos al usuario ambos n�meros y los almacenamos
		sc.close();
		//Cerramos teclado
		
		if (num1 <= num2) {
			//Realizamos comprobaci�n de qu� n�mero es mayor
			System.out.println(
					"Entre el n�mero " + num1 + " y el n�mero " + num2 + " existen los siguiente n�meros pares: ");
			for (i = num1; i <= num2; i++) {
				//Cuando el num1 es mayor, lo tomamos como valor base hasta llegar al num2
				if (i % 2 == 0) {
					//Comprobamos si es par (es decir, si al dividir por 2 su resto es 0) y si lo es, imprimimos el resultado
					System.out.println(i);
				}
			}
		} else {
			System.out.println(
					"Entre el n�mero " + num2 + " y el n�mero " + num1 + " existen los siguiente n�meros pares: ");
			for (i = num2; i <= num1; i++) {
				//Cuando el num2 es mayor, lo tomamos como valor base hasta llegar al num1
				if (i % 2 == 0) {
					System.out.println(i);
					//Comprobamos si es par (es decir, si al dividir por 2 su resto es 0) y si lo es, imprimimos el resultado
					
				}

			}

		}

	}
	
}
