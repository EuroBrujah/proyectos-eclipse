package bucles;

import java.util.Scanner;

public class TablaMultiplicar {

	public static void main(String[] args) {
		// Preguntamos al usuario un n�mero e imprimimos esa tabla de multiplicar.
		Scanner sc = new Scanner(System.in);
		// Declaramos Scanner.

		System.out.println("Introduzca un n�mero para ver la tabla de multiplicar del mismo: ");
		int num = sc.nextInt();
		//Solicitamos al usuario un n�mero y lo almacenamos.
		for (int i = 0; i < 11; i++) {
			System.out.println(""+num+" x "+i+": \n"+num * i);
		}
		//Hacemos que el bucle corra de 0 a <10, multiplicando la iteraci�n del bucle por el n�mero introducido
		//imprimiendo los resultados.
		sc.close();
		//Cerramos Scanner.
	}

}
