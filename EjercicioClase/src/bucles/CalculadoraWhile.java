package bucles;

import java.util.Scanner;

public class CalculadoraWhile {

	public static void main(String[] args) {
		// Programa que introduzca dos n�meros por teclado y un car�cter y realiza la
		// operacion correspondiente
		Scanner sc = new Scanner(System.in);
		// Inicializamos el teclado
		char seguir = 'S';

		while (seguir == 'S') {

			System.out.println("Introduzca el primer t�rmino de la operaci�n: ");
			double n1 = sc.nextDouble();
			// Solicitamos y almacenamos el primer n�mero de la operaci�n
			System.out.println("Ahora introduzca el tipo de operaci�n a realizar (+, -, *, /): ");
			String op = sc.next();
			// Solicitamos y almacenamos un char como tipo de operaci�n
			System.out.println("Introduzca el segundo t�rmino de la operaci�n: ");
			double n2 = sc.nextDouble();
			// Solicitamos y almacenamos el segundo n�mero de la operaci�n
			char operador = op.charAt(0);
			// Leemos el primer caracter de la string y lo almacenamos
			switch (operador) {
			case '+':
				double result = n1 + n2;
				System.out.println("El resultado de sumar " + n1 + " y " + n2 + " es " + result + ".");
				break;
			// Si el primer caracter de la string es +, realizamos la suma e imprimimos.
			// Usamos break para salir.
			case '-':
				result = n1 - n2;
				System.out.println("El resultado de restar " + n1 + " y " + n2 + " es " + result + ".");
				break;
			// Si el primer caracter de la string es -, realizamos la resta e imprimimos.
			// Usamos break para salir.
			case '*':
				result = n1 * n2;
				System.out.println("El resultado de multiplicar " + n1 + " y " + n2 + " es " + result + ".");
				break;
			// Si el primer caracter de la string es *, realizamos la multiplicaci�n e
			// imprimimos. Usamos break para salir.
			case '/':
				result = n1 / n2;
				System.out.println("El resultado de dividir " + n1 + " y " + n2 + " es " + result + ".");
				break;
			// Si el primer caracter de la string es /, realizamos la divisi�n e imprimimos.
			// Usamos break para salir.
			default:
				System.out.println("Reinicie el programa e introduzca uno de los caracteres permitidos.");
				break;
			// Si el primer caracter de la string no es ninguno de los permitidos, le
			// solicitamos que introduzca otro. Usamos break para salir.

			}
			System.out.println("�Quiere seguir usando la calculadora? (S/N)");
			seguir = sc.next().charAt(0);
		}

		sc.close();
		// Cerramos teclado

	}

}
