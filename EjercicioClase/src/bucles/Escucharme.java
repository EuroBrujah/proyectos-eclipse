package bucles;

import java.util.Scanner;

public class Escucharme {

	public static void main(String[] args) {
		// Programa que lee numeros positivos hasta que el usuario introduzca un n�mero
		// negativo y entonces calcula la media.
		Scanner sc = new Scanner(System.in);
		// Inicializamos teclado
		int cont = 0;
		int numFinal = 0;
		int num;
		double media;
		// Declaramos variables

		do {
			// El bucle corre mientras el usuario introduce n�meros positivos.
			System.out.println(
					"Introduzca un n�mero entero.\nIntroducir un negativo realiza la media de los n�meros introducidos y finaliza el programa");
			num = sc.nextInt();
			// Solicitamos el n�mero entero y lo almacenamos.
			if (num >= 0) {
				numFinal += num;
				cont++;
				// Si el n�mero es positivo, lo almacenamos y aumentamos el contador.
			}
		} while (num >= 0);
		media = (double) Math.round(((double) numFinal / cont)*100)/100;
		// Calculamos la media.
		System.out.println("La media de los n�meros introducidos es " + media);
		sc.close();
		// Imprimimos el resultado y cerramos teclado.

	}
}
