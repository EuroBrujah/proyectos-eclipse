package bucles;

import java.util.Scanner;

public class Login {

	public static void main(String[] args) {
		// Programa login
		Scanner sc = new Scanner(System.in);
		// Inicializamos Scanner
		final String PASSWORD = "123";
		String pass;
		//Declaramos la pass correcta y la variable donde vamos a almacenar la que introduzca el usuario

		do {
			System.out.println("Introduzca su contraseņa: ");
			pass = sc.next();
			if (!pass.equals(PASSWORD)) {
				System.out.println("Contraseņa incorrecta.");
			}
			//Solicitamos al usuario la contraseņa y la almacenamos
		} while (!pass.equals(PASSWORD));
		//Comparamos ambas contraseņas, si es correcta finaliza el bucle
		System.out.println("Contraseņa correcta. Bienvenido.");
		sc.close();
		//Mensaje final y cierre de teclado.
	}
}
