package bucles;

import java.util.Scanner;

public class Ahorcado {

	public static void main(String[] args) {
		/* Solicita una frase y la muestra oculta tras cada jugada, conforme se aciertan
		* caracteres se van revelando,
		* cada vez que el usuario falla se descuenta un intento.
		*/
		Scanner sc = new Scanner(System.in);
		//Declaramos teclado
		System.out.println("Introduzca una frase: ");
		StringBuffer adivina = new StringBuffer(sc.next());
		//Solicitamos al usuario una frase y la almacenamos
		StringBuffer palabra = new StringBuffer("");

		for (int j = 0; j < adivina.length(); j++) {
			palabra.insert(j, "*");
		}
		String ahorcado = "";
		int intentos = 5;
		boolean bool = false;

		do {
			System.out.println("Introduzca un caracter: ");
			char letra = sc.next().charAt(0);
			for (int i = 0; i < adivina.length(); i++) {
				if (letra == adivina.charAt(i)) {
					palabra.delete(i, i + 1);
					palabra.insert(i, letra);
					bool = true;
					}

			}if (!bool /*|| palabra.contains(Character.toString(letra)))*/ && intentos == 5) {
				intentos--;
				System.out.println("No existe la letra. Te quedan " + intentos + " intentos.");
				ahorcado = "|\n|\n|\n______";
				System.out.println(ahorcado);
			}else if (!bool && intentos == 4) {
				intentos--;
				System.out.println("No existe la letra. Te quedan " + intentos + " intentos.");
				ahorcado = "-----\n|\n|\n|\n______";
				System.out.println(ahorcado);
			}else if (!bool && intentos == 3) {
				intentos--;
				System.out.println("No existe la letra. Te quedan " + intentos + " intentos.");
				ahorcado = "-----\n|    O\n|\n|\n______";
				System.out.println(ahorcado);
			}else if (!bool && intentos == 2) {
				intentos--;
				System.out.println("No existe la letra. Te quedan " + intentos + " intentos.");
				ahorcado = "-----\n|    O\n|    T\n|\n______";
				System.out.println(ahorcado);
			}else if (!bool && intentos == 1) {
				intentos--;
				System.out.println("No existe la letra. Te quedan " + intentos + " intentos.");
			}
			System.out.println("Palabra: "+palabra);
			bool = false;

		} while (intentos > 0 && !palabra.toString().equals(adivina.toString()));
		if(palabra.toString().equals(adivina.toString())) {
			System.out.println("Enhorabuena, la palabra era "+palabra+".");
		}else {
			System.out.println("Lo siento, se te han acabado los intentos.");
			ahorcado = "-----\n|    O\n|    T\n|    ^\n______";
			System.out.println(ahorcado);
			System.out.println("La palabra era "+adivina+".");
		}

		sc.close();
	}
}
