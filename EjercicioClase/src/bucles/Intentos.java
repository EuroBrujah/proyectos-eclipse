package bucles;

import java.util.Random;
import java.util.Scanner;

public class Intentos {

	public static void main(String[] args) {
		// Se genera un n�mero aleatorio entre 1 y 9 y el usuario tiene 3 intentos para adivinar ese n�mero.
		Scanner sc = new Scanner(System.in);
		//Inicializamos teclado.
		Random rand = new Random();
		//Importamos un rand.
		int adivina = 1 + rand.nextInt(9);
		//Hacemos que genere un n�mero de 1 a 9 y lo almacenamos en una variable.
		int intento;
		//Variable para almacenar los intentos del usuario.
		int cont = 0;
		//Contador para el n�mero de intentos.
		
		do{
			System.out.println("�Qu� n�mero estoy pensando?");
			intento = sc.nextInt();
			//Solicitamos al usuario su primera respuesta y la almacenamos.
			cont++;
			//Aumentamos el contador de intentos
			if(intento==adivina){
				System.out.println("Enhorabuena, has acertado.");
				//Si acierta, imprimimos el mensaje.
			}else if (cont==3){
				System.out.println("Es incorrecto, se han agotado los intentos.");
				//Si se le agotan los intentos, imprimimos el mensaje
			}else {
				if (intento>adivina) {
				System.out.println("Es incorrecto, el n�mero a adivinar es menor que el introducido. Int�ntelo de nuevo. Le quedan "+(3-cont)+" intentos.");
			}else {
				System.out.println("Es incorrecto, el n�mero a adivinar es mayor que el introducido. Int�ntelo de nuevo. Le quedan "+(3-cont)+" intentos.");
			//En los otros casos, imprimimos si el n�mero objetivo es mayor o menor al introducido y el n�mero de intentos restantes.
			}
			}
		}while (intento!=adivina && cont<3);
		//El bucle acaba cuando el usuario adivina el n�mero o el contador alcanza 3.
		
		sc.close();
		//Cerramos teclado
		

	}

}
